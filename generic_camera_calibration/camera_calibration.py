# Copyright (C) 2021  David Uhlig
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
from numba import cuda
from numpy.core.multiarray import ndarray
from tqdm import tqdm

from generic_camera_calibration import calibration_utilities as utilities
from generic_camera_calibration import camera_ray_estimation as rays
from generic_camera_calibration import pose_estimation as pose
from generic_camera_calibration.pose_estimation import log_map_SO3, exp_map_so3
from generic_camera_calibration.calibration_utilities import eul2rot


def calibrate(x_ik: ndarray,
              w_ik: ndarray = None,
              R_k: ndarray = None,
              t_k: ndarray = None,
              r_i: ndarray = None,
              max_num_iterations: int = 500,
              estimate_monitor_model: bool = False,
              use_acceleration: bool = False,
              use_cuda: bool = True,
              calc_errors: bool = True,
              save_errors: bool = False,
              show_progress: bool = False) -> dict:
    """Alternating minimization-based calibration. Generalized camera calibration followed by a generalized
    pose estimation to calibrate a camera from unknown point correspondences, with measurement uncertainty.

    Args:
        x_ik: 3D points corresponding to i-th ray and k-th pose as (3,i,k)-array.
        w_ik: Weight factor of each point. If no weight is given, they are set to 1.
        R_k: Estimate of k rotations in (3,3,k)-array.
        t_k: Estimate of k translations in(3,1,k)-array.
        r_i: (6,i)-array of rays r_i = (d_i, m_i).
        max_num_iterations: Maximum number of iterations.
        estimate_monitor_model: Estimate monitor shape during calibration.
        use_acceleration: Use Nesterov-based acceleration.
        use_cuda: Execute calculations on the GPU.
        calc_errors: Calculate and print current optimization error.
        save_errors: Save errors to result.
        show_progress: Show 3D plot of current estimate.

    Returns:
        Dictionary containing calibration result: rays "r_i", rotation "R_k", translation "t_k".
        If save_errors==True, the dictionary also contains the arrays "mean_euclidean_error"
        and "mean_weighted_error" which contain the mean error for each iteration,
        and the final euclidean distance of every point-ray correspondence "error".
    """
    num_rays = x_ik.shape[0]
    num_poses = x_ik.shape[1]

    if w_ik is None:
        w_ik = np.ones((num_rays, num_poses))

    # initialize algorithm with pose estimate if no rays are given
    estimate_ray_parameters = r_i is None
    if estimate_ray_parameters:
        r_i = np.zeros((num_rays, 6), dtype=np.float64)  # use float64 to avoid numerical issues
        # r_i = np.zeros((num_rays, 4), dtype=np.float64)  # use float64 to avoid numerical issues
        if R_k is None:
            # # initialize with identity matrix?
            # R_k = np.repeat(np.eye(3)[:, :, None], axis=2, repeats=num_poses)
            # initialize with random rotation?
            R_k = np.asarray([eul2rot(2*np.pi*np.random.randn(3, 1)) for k in range(num_poses)])
        if t_k is None:
            # # initialize with increasing distance?
            # t_k = np.zeros((num_poses, 3, 1))
            # # x,y-coordinates approximately correspond to weighted mean of observed coordinates
            # t_k[:, 0, 0] = -np.mean(x_ik[:, :, 0]*w_ik, axis=0)/np.mean(w_ik, axis=0)
            # t_k[:, 1, 0] = -np.mean(x_ik[:, :, 1]*w_ik, axis=0)/np.mean(w_ik, axis=0)
            # # approximately estimate depth (define each depth or use an increasing depth
            # # or use weighted variance or TODO: whatever...)
            # t_k[:, 2, 0] = np.linspace(0,1, num_poses)
            # initialize with random translation?
            t_k = np.asarray([x_ik.std(axis=(0, 1))*np.random.randn(3) for k in range(num_poses)])[:, :, None]

    # make copy to track parameter convergence
    R_k_1 = R_k.copy()
    t_k_1 = t_k.copy()

    if use_acceleration:
        acceleration_start = 5
        # Nesterov's acceleration scheme
        alpha_n_1 = alpha_n = 1.0
        # restart parameters
        eta = 1.0
        c_n_1 = 1e99

    if save_errors:
        # initialize error metrics
        e_mean_euclidean = np.zeros(max_num_iterations)
        e_mean_weighted = np.zeros(max_num_iterations)
        calc_errors = True

    if use_cuda:
        r_i = cuda.to_device(r_i)  # use float64 for numerical stability!
        x_ik = cuda.to_device(np.ascontiguousarray(np.float32(x_ik)))  # float32 to save VRAM...
        w_ik = cuda.to_device(np.ascontiguousarray(np.float32(w_ik)))  # float32 to save VRAM...

    # Start calibration
    t_range = tqdm(range(max_num_iterations), leave=True, desc='Calibration')
    for n in t_range:
        desc = 'Calibration: '

        # estimate camera rays
        if estimate_ray_parameters:
            rays.estimate(r_i, x_ik, w_ik, R_k, t_k, method='p2l_edist' + use_cuda*'_cuda')
        estimate_ray_parameters = True

        # estimate calibration target pose
        err = pose.estimate(r_i, x_ik, w_ik, R_k, t_k, method='least_squares')
        # e_mean_obj = np.mean(err)
        # e_std_obj = np.std(err)
        # desc += f'Obj {e_mean_obj:1.1e}(±{e_std_obj:1.1e}). '

        # calculate errors
        if calc_errors:
            # calculate ray object distance (euclidean)
            e_mean = utilities.get_ray_distance_euclidean(r_i, x_ik, R_k, t_k,
                                                          calc_mean=True, use_cuda=use_cuda)
            desc += f'Euclidean {e_mean*1e6:1.4f}µm. '

            # calculate ray object distance (weighted euclidean)
            e_mean_w = utilities.get_ray_distance_euclidean(r_i, x_ik, R_k, t_k, w_ik=w_ik,
                                                            calc_mean=True, use_cuda=use_cuda)
            desc += f'Weighted {e_mean_w*1e6:1.4f}µm. '

            if save_errors:
                e_mean_euclidean[n] = e_mean
                e_mean_weighted[n] = e_mean_w

        t_range.set_description(desc=desc)

        # accelerated alternating minimization
        if use_acceleration:
            if n >= acceleration_start:
                # calculate change of translation
                delta_t_k = (t_k - t_k_1)
                # calculate change of rotation in so(3)
                delta_omega_k = log_map_SO3(np.einsum('kvu, kvs -> kus', R_k_1, R_k))  # R_k_1.T@R_k

                # restart acceleration? check if there is too much change
                c_n = np.sum(delta_t_k**2) + np.sum(delta_omega_k**2)
                if (c_n > eta*c_n_1) and c_n > 1e-4:
                    beta_n = 0
                    alpha_n_1 = alpha_n = 1.0
                    c_n_1 = 1/eta*c_n_1
                else:
                    alpha_n = (1.0 + np.sqrt(1.0 + 4*alpha_n_1**2))/2.0
                    beta_n = 0.5*(alpha_n_1 - 1.0)/alpha_n
                    c_n_1 = c_n
                    # update acceleration parameter
                    alpha_n_1 = alpha_n

                # update translation
                t_k = t_k + beta_n*delta_t_k

                # update rotation, same as for translation, but evaluated on SO(3)
                for k in range(R_k.shape[0]):
                    R_k[k, :, :] = exp_map_so3(beta_n*delta_omega_k[k, :])@R_k[k, :, :]
                    # force projection onto SO(3) to avoid accumulation of numerical errors
                    U, _, Vh = np.linalg.svd(R_k[k, :, :])
                    R_k[k, :, :] = U@np.diag([1, 1, np.linalg.det(Vh@U)])@Vh

        # check parameter convergence
        delta_R = np.asarray([log_map_SO3(R_k_1[k, :, :].T@R_k[k, :, :]) for k in range(num_poses)])
        delta_t = t_k - t_k_1

        # save last step
        t_k_1 = t_k.copy()
        R_k_1 = R_k.copy()

        # convergence? TODO: how to set tolerance!? Error convergence? Ray convergence or too much memory needed?
        t_tol = 1e-9
        r_tol = 1e-8
        if np.mean(np.abs(delta_t)) < t_tol and np.mean(np.abs(delta_R)) < r_tol:
            break

        if show_progress:
            # show 3D plot of estimated reference points
            if use_cuda:
                utilities.show_result(r_i.copy_to_host(), x_ik.copy_to_host(), R_k, t_k,
                                      show_rays=False, show_reference=True, num_rays=200, figure_name='Estimated')
            else:
                utilities.show_result(r_i, x_ik, R_k, t_k,
                                      show_rays=False, show_reference=True, num_rays=200, figure_name='Estimated')

    if use_cuda:
        # copy back to cpu
        r_i = r_i.copy_to_host()

    result = {"r_i": r_i, "R_k": R_k, "t_k": t_k}

    if save_errors:
        # mean errors for each iteration
        result["mean_euclidean_error"] = e_mean_euclidean
        result["mean_weighted_error"] = e_mean_weighted
        # final euclidean distance of every point-ray correspondence
        error_array = utilities.get_ray_distance_euclidean(r_i, x_ik, R_k, t_k, calc_mean=False, use_cuda=use_cuda)
        result["error"] = error_array

    return result
