# Copyright (C) 2021  David Uhlig
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import math
import os
import re
import warnings
from typing import Optional, Tuple

import matplotlib.pyplot as plt
import numpy as np
import numpy.linalg as linalg
from mpl_toolkits.mplot3d import Axes3D
from numba import njit, prange, cuda, float64
from numba.cuda import devicearray
from numpy.core.multiarray import ndarray


#%% Error calculations
# euclidean distance
def get_ray_distance_euclidean(r_i: ndarray,
                               x_ik: ndarray,
                               R_k: ndarray,
                               t_k: ndarray,
                               w_ik: Optional[ndarray] = None,
                               calc_mean: Optional[bool] = False,
                               use_cuda: Optional[bool] = False) -> ndarray:
    """Calculates the (weighted) distance from all points to its associated ray.

    Args:
        r_i: (6,i)-array of rays r_i = (d_i, m_i).
        x_ik: 3D points corresponding to i-th ray and k-th pose as (3,i,k)-array.
        R_k: Estimate of k rotations in (3,3,k)-array.
        t_k: Estimate of k translations in(3,1,k)-array.
        w_ik: Weight factor for every correspondence for weighted mean calculation.
        calc_mean: Only return the mean distance.
        use_cuda: Calculate distances on cpu or cuda gpu.
    Returns:
        Array of distances for every ray-point correspondence. Or mean distance as float value.
    """
    if not use_cuda:
        # calculate distance
        if x_ik.shape[2] == 2:
            distance = _ray_point_euclidean_dist(r_i, np.einsum('kuv, ikv -> iku', R_k[:, :, 0:2], x_ik) + t_k[:, :, 0])
        else:
            distance = _ray_point_euclidean_dist(r_i, np.einsum('kuv, ikv -> iku', R_k, x_ik) + t_k[:, :, 0])
        if w_ik is not None:
            distance *= w_ik
        if calc_mean:
            distance = np.mean(distance)
            if w_ik is not None:
                distance /= np.mean(w_ik)
    else:
        # get cuda parameters
        device = cuda.get_current_device()
        tpb = device.WARP_SIZE
        n = x_ik.shape[0]
        bpg = int(np.ceil(float(n)/tpb))
        # calculate distance
        if w_ik is None:
            w_ik = np.ones((x_ik.shape[0], x_ik.shape[1]))
        distance = np.zeros((x_ik.shape[0], x_ik.shape[1]), dtype=np.float32)
        _ray_point_euclidean_dist_cuda[bpg, tpb](r_i, x_ik, w_ik, R_k, t_k, distance)
        if calc_mean:
            distance = np.mean(distance)/np.mean(w_ik)
    return distance


@njit
def _ray_point_euclidean_dist(r_i: ndarray,
                              p_ik: ndarray) -> ndarray:
    """Calculates euclidean distance from all rays to all points.

    Args:
        r_i: Camera rays.
        p_ik: Observed points.

    Returns:
        Distance of ray to every point.
    """
    distance = np.zeros((p_ik.shape[0], p_ik.shape[1]), dtype=r_i.dtype)
    for i in prange(p_ik.shape[0]):
        # get ray parameter
        d_i, m_i = get_direction_moment_vector(r_i[i, :])
        for k in prange(p_ik.shape[1]):
            distance[i, k] = (p_ik[i, k, 1]*d_i[2] - p_ik[i, k, 2]*d_i[1] - m_i[0])**2 + \
                             (p_ik[i, k, 2]*d_i[0] - p_ik[i, k, 0]*d_i[2] - m_i[1])**2 + \
                             (p_ik[i, k, 0]*d_i[1] - p_ik[i, k, 1]*d_i[0] - m_i[2])**2
            distance[i, k] = np.sqrt(distance[i, k])
    return distance


@cuda.jit
def _ray_point_euclidean_dist_cuda(r_i: devicearray,
                                   x_ik: devicearray,
                                   w_ik: devicearray,
                                   R_k: devicearray,
                                   t_k: devicearray,
                                   distance: devicearray):
    """Calculates the distance for all points to its corresponding ray using cuda grafic card.

    Args:
        r_i: (6,i)-array of rays r_i = (d_i, m_i).
        x_ik: 3D points corresponding to i-th ray and k-th pose as (3,i,k)-array.
        w_ik: Weight factor of each point.
        R_k: Estimate of k rotations in (3,3,k)-array.
        t_k: Estimate of k translations in(3,1,k)-array.
        distance: Distance for every ray-point correspondence as cuda.devicearray.
    """
    i = cuda.grid(1)
    if i < r_i.shape[0]:
        # get ray parameter
        d_i, m_i = get_direction_moment_vectors_cuda(r_i[i, :])

        for k in range(x_ik.shape[1]):
            # Transform point coordinates p_ik = R_k@x_ik + t_k
            p_ik0 = R_k[k, 0, 0]*x_ik[i, k, 0] + R_k[k, 0, 1]*x_ik[i, k, 1] + t_k[k, 0, 0]
            p_ik1 = R_k[k, 1, 0]*x_ik[i, k, 0] + R_k[k, 1, 1]*x_ik[i, k, 1] + t_k[k, 1, 0]
            p_ik2 = R_k[k, 2, 0]*x_ik[i, k, 0] + R_k[k, 2, 1]*x_ik[i, k, 1] + t_k[k, 2, 0]
            if x_ik.shape[2] == 3:
                p_ik0 += R_k[k, 0, 2]*x_ik[i, k, 2]
                p_ik1 += R_k[k, 1, 2]*x_ik[i, k, 2]
                p_ik2 += R_k[k, 2, 2]*x_ik[i, k, 2]
            # Calculate error
            dist = (p_ik1*d_i[2] - p_ik2*d_i[1] - m_i[0])**2 + \
                   (p_ik2*d_i[0] - p_ik0*d_i[2] - m_i[1])**2 + \
                   (p_ik0*d_i[1] - p_ik1*d_i[0] - m_i[2])**2
            dist = math.sqrt(dist)

            distance[i, k] = w_ik[i, k]*dist


@njit
def get_direction_moment_vector(r_i):
    """Returns ray direction and ray moment vector. When the rays are in
    the the 4-parameter representation, the missing values are calculated.

    Args:
        r_i: (i,6) or (i,4)- array of rays.

    Returns:
        Tuple of ray direction and ray moment vectors.
    """
    # get ray parameter
    if r_i.ndim == 1:
        d_i = np.zeros(3, dtype=r_i.dtype)
        m_i = np.zeros(3, dtype=r_i.dtype)
    else:
        d_i = np.zeros(r_i.shape[0:-1] + (3,), dtype=r_i.dtype)
        m_i = np.zeros(r_i.shape[0:-1] + (3,), dtype=r_i.dtype)
    if r_i.shape[-1] == 6:
        d_i = r_i[..., 0:3]
        m_i = r_i[..., 3:6]
    elif r_i.shape[-1] == 4:
        d_i[..., 0:2] = r_i[..., 0:2]
        m_i[..., 0:2] = r_i[..., 2:4]
        # calculate remaining parameters
        # d_i[2] by definition should be positive
        d_i[..., 2] = np.sqrt(np.maximum(1 - d_i[..., 0]**2 - d_i[..., 1]**2, 0))
        if r_i.ndim == 1:
            if d_i[2] != 0:
                m_i[2] = -(d_i[0]*m_i[0] + d_i[1]*m_i[1])/d_i[2]
            else:
                m_i[2] = 0  # some arbitrary value
        else:
            m_i[..., 2] = np.where(d_i[..., 2] != 0,
                                   -(d_i[..., 0]*m_i[..., 0] + d_i[..., 1]*m_i[..., 1])/d_i[..., 2],
                                   0.0)
    return d_i, m_i


@cuda.jit(device=True)
def get_direction_moment_vectors_cuda(r_i):
    """Returns ray direction and ray moment vector. When the rays are in
    the 4-parameter representation, the missing values are calculated.

    Args:
        r_i: (i,6) or (i,4)- array of rays.

    Returns:
        Tuple of ray direction and ray moment vectors.
    """
    # get ray parameter
    d_i = cuda.local.array(3, dtype=float64)
    m_i = cuda.local.array(3, dtype=float64)
    d_i[0] = r_i[0]
    d_i[1] = r_i[1]
    if r_i.shape[-1] == 6:
        d_i[2] = r_i[2]
        m_i[0] = r_i[3]
        m_i[1] = r_i[4]
        m_i[2] = r_i[5]
    elif r_i.shape[-1] == 4:
        # calculate remaining parameters
        d_i[2] = 1 - d_i[0]**2 - d_i[1]**2
        d_i[2] = math.sqrt(d_i[2]*(d_i[2] > 0))  # d_i[2] by definition should be positive
        m_i[0] = r_i[2]
        m_i[1] = r_i[3]
        if d_i[2] != 0:
            m_i[2] = -(d_i[0]*m_i[0] + d_i[1]*m_i[1])/d_i[2]
        else:
            m_i[2] = 0  # some arbitrary value
    return d_i, m_i


#%% normalization of camera ambiguities
def normalize_coordinate_system(r_i: ndarray,
                                R_k: ndarray = None,
                                t_k: ndarray = None,
                                w_i: ndarray = None,
                                sensor_shape: ndarray = None):
    """Shift coordinate system into point that is point closest to all r_i.
    Rotate coordinate system, so that z-axis points in main ray direction.
    The input rays as well the input rotation and translation will be modified!

    Args:
        r_i: (i,6)-array of rays r_i = (d_i, m_i).
        R_k: Estimate of k rotations in (k,3,3)-array.
        t_k: Estimate of k translations in(k,3,1)-array.
        w_i: Weights.
    """

    # Calculate mean ray direction
    d_mean = _calc_mean_direction(r_i, w_i)
    d_mean /= np.linalg.norm(d_mean)

    # transform mean direction into a rotation matrix
    v = np.cross(d_mean, np.array([0, 0, 1]))
    s = np.linalg.norm(v)
    if s == 0:
        R_o = np.eye(3)
    else:
        c = np.dot(d_mean, np.array([0, 0, 1]))
        if abs(c + 1) < 1e-16:
            R_o = np.eye(3) + hat(v)
        else:
            R_o = np.eye(3) + hat(v) + hat(v)@hat(v)/(1 + c)

    # Calculate point that's closest to all r_i
    x_o = _calc_origin(r_i, w_i)

    # Shift origin
    r_i = _translate_rays(r_i, -x_o)
    if t_k is not None:
        t_k[:, :, :] = t_k - x_o[:, np.newaxis]

    # Rotate rays to point in z-direction
    r_i = _rotate_rays(r_i, R_o)
    if R_k is not None:
        R_k[:, :, :] = np.einsum('uv, kvs -> kus', R_o, R_k)
    if t_k is not None:
        t_k[:, :, :] = np.einsum('uv, kvs -> kus', R_o, t_k)

    # Change ray direction to point towards positive z-direction
    r_i[:, :] = r_i[:, :]*np.sign(r_i[:, 2])[:, np.newaxis]

    # Check if most calibration patterns are in front of camera (positive z-axis)
    # TODO: check if all reference points are in front of the camera?
    if (t_k is not None) and (np.median(t_k[:, 2, 0]) < 0):
        # rotate 180° around x-axis
        R_o = eul2rot([np.pi, 0, 0])
        if R_k is not None:
            R_k[:, :, :] = np.einsum('uv, kvs -> kus', R_o, R_k)
        t_k[:, :, :] = np.einsum('uv, kvs -> kus', R_o, t_k)
        r_i = _rotate_rays(r_i, R_o)

    # Change ray direction to point towards positive z-direction
    r_i[:, :] = r_i[:, :]*np.sign(r_i[:, 2])[:, np.newaxis]

    # Get rotation angle around z-axis
    if sensor_shape is None:
        # use PCA for rotation angle
        alpha = _calc_z_rotation_pca(r_i, w_i)
    else:
        # use gradient along sensor directions
        alpha = _calc_z_rotation_sensor(r_i[:, 0:3], sensor_shape, w_i)

    # rotate around z-axis to align main orientation with coordinate axis
    R_o = eul2rot([0, 0, alpha])
    r_i = _rotate_rays(r_i, R_o)
    if R_k is not None:
        R_k[:, :, :] = np.einsum('uv, kvs -> kus', R_o, R_k)
    if t_k is not None:
        t_k[:, :, :] = np.einsum('uv, kvs -> kus', R_o, t_k)

    return r_i


def _rotate_rays(r_i, R):
    """Rotates set of Plücker-rays.

    Args:
        r_i: ray parameters.
        R: Rotation matrix.

    Returns:
        Rotated rays.
    """
    r = np.zeros_like(r_i)
    r[:, 0:3] = np.einsum('uv, iv -> iu', R, r_i[:, 0:3])
    r[:, 3:6] = np.einsum('uv, iv -> iu', R, r_i[:, 3:6])
    return r


def _translate_rays(r_i, t):
    """Translates set of Plücker-rays.

    Args:
        r_i: ray parameters.
        t: translation vector.

    Returns:
        Translated rays.
    """
    r = r_i.copy()
    r[:, 3:6] = np.einsum('uv, iv -> iu', hat(t), r_i[:, 0:3]) + r_i[:, 3:6]
    return r


@njit
def _calc_origin(r_i: ndarray, w_i: ndarray = None) -> ndarray:
    """Calculates point that is closest to all (weighted) rays.
    minimize sum_i wi*|| cross(x,di) - mi||**2

    Args:
        r_i: Camera rays.
        w_i: Weights.

    Returns:
        3D point.
    """
    x = np.zeros(3, dtype=np.float64)
    dxdx = np.zeros((3, 3), dtype=np.float64)
    for i in prange(r_i.shape[0]):
        d_i, m_i = get_direction_moment_vector(r_i[i, :])
        # Calc screw matrix [di]x = Dix
        Dix = np.zeros((3, 3), dtype=np.float64)
        Dix[1, 0] = d_i[2]
        Dix[2, 0] = -d_i[1]
        Dix[0, 1] = -d_i[2]
        Dix[2, 1] = d_i[0]
        Dix[0, 2] = d_i[1]
        Dix[1, 2] = -d_i[0]

        # (weighted) sum
        if w_i is None:
            x += Dix@m_i
            dxdx += Dix@Dix.T
        else:
            x += w_i[i]*Dix@m_i
            dxdx += w_i[i]*Dix@Dix.T
    # get best origin
    return linalg.solve(dxdx, x)


@njit
def _calc_mean_direction(r_i: ndarray, w_i: ndarray = None) -> ndarray:
    """Calculates the (weighted) average direction of the input rays.
    maximize sum_i wi*<x, di>**2 + lambda*(||x|| -  1)

    Args:
        r_i: Camera rays.
        w_i: Weights.

    Returns:
        3D point.
    """
    didi = np.zeros((3, 3), dtype=np.float64)
    for i in prange(r_i.shape[0]):
        # Calc wi*di.T@di
        if w_i is None:
            didi += np.outer(r_i[i, 0:3], r_i[i, 0:3])
        else:
            didi += w_i[i]*np.outer(r_i[i, 0:3], r_i[i, 0:3])
    # get eigenvector corresponding to maximal eigenvalue
    eig_value, eig_vector = linalg.eigh(didi)
    return eig_vector[:, np.argmax(eig_value)]


def _calc_z_rotation_pca(r_i: ndarray, w_i: ndarray = None) -> ndarray:
    """Calculates the angle that aligns the x-y-axis with the sensor x-y-axis.

    Args:
        r_i: Camera rays.
        w_i: Weights.

    Returns:
        Angle.
    """

    # Get intersection with z==100000
    z_0 = np.cross(r_i[:, 0:3], r_i[:, 3:6])
    z_0 += ((100000.0 - z_0[:, 2])/r_i[:, 2])[:, np.newaxis]*r_i[:, 0:3]

    # z_0 = r_i[:, 0:2]/r_i[:, 2, None]

    # Do PCA on x-,y-components to obtain orientation
    z_0 = z_0[:, 0:2]

    if w_i is None:
        # zero mean
        z_0 -= np.mean(z_0, axis=0, keepdims=True)
        # get covariance matrix
        cov = np.einsum('ia,ib -> ab', z_0, z_0)/z_0.shape[0]
    else:
        # zero mean
        z_0 -= np.mean(z_0*w_i[:, np.newaxis], axis=0, keepdims=True)
        # get covariance matrix
        cov = np.einsum('i,ia,ib -> ab', w_i, z_0, z_0)

    _, eig_vectors = linalg.eig(cov)
    e0 = eig_vectors[:, 0]
    e1 = eig_vectors[:, 1]
    angle = np.arctan2(e1[0], e1[1])
    return angle


def _calc_z_rotation_sensor(d_i: ndarray, sensor_shape: ndarray, w_i: ndarray = None) -> ndarray:
    """Calculates the angle that aligns the x-y-axis with the sensor x-y-axis.

    Args:
        d_i: Ray directions.
        w_i: Weights.

    Returns:
        Angle.
    """

    # reshape x- and y-direction to sensor image
    x = d_i[:, 0].reshape(sensor_shape)
    y = d_i[:, 1].reshape(sensor_shape)

    # calculate image gradient
    grad_x = np.asarray(np.gradient(x))
    grad_y = np.asarray(np.gradient(y))

    if w_i is None:
        grad_x_mean = np.nanmean(grad_x, axis=(1, 2))
        grad_y_mean = np.nanmean(grad_y, axis=(1, 2))
    else:
        w_i = w_i.reshape(sensor_shape)
        grad_x_mean = np.nanmean(grad_x*w_i, axis=(1, 2))
        grad_y_mean = np.nanmean(grad_y*w_i, axis=(1, 2))

    alpha1 = np.arctan2(grad_x_mean[0], grad_x_mean[1]) + np.pi
    alpha2 = np.arctan2(grad_y_mean[0], grad_y_mean[1]) + np.pi/2
    # mean angle
    alpha = np.arctan2(0.5*(np.sin(alpha1) + np.sin(alpha2)), 0.5*(np.cos(alpha1) + np.cos(alpha2)))
    alpha %= 2*np.pi

    return alpha

#%% transformations
def eul2rot(eul):
    """Calculates Rotation Matrix given euler angles.

    Parameters
    ----------
    eul: Euler angle

    Returns
    -------
    Rotation Matrix

    """
    r_x = np.array([[              1,               0,               0],
                    [              0,  np.cos(eul[0]), -np.sin(eul[0])],
                    [              0,  np.sin(eul[0]),  np.cos(eul[0])]
                    ], dtype=np.float64)
    r_y = np.array([[ np.cos(eul[1]),               0,  np.sin(eul[1])],
                    [              0,               1,               0],
                    [-np.sin(eul[1]),               0,  np.cos(eul[1])]
                    ], dtype=np.float64)
    r_z = np.array([[ np.cos(eul[2]), -np.sin(eul[2]),               0],
                    [ np.sin(eul[2]),  np.cos(eul[2]),               0],
                    [              0,               0,               1]
                    ], dtype=np.float64)

    rot = r_z @ r_y @ r_x
    return rot


def rot2eul(R):
    """Calculates rotation matrix to euler angles
       The result is the same as MATLAB except the order
       of the euler angles ( x and z are swapped ).

    Parameters
    ----------
    rot: Rotation matrix

    Returns
    -------
    Euler angles
    """

    if R.ndim == 2:
        rot = R
        # Checks if a matrix is a valid rotation matrix. Transpose of rot should be inverse.
        # assert (linalg.norm(np.eye(3, dtype=rot.dtype) - rot.T @ rot) < 1e-6)
        if linalg.norm(np.eye(3, dtype=rot.dtype) - rot.T @ rot) > 1e-6:
            warnings.warn('Rotation matrix deviates from orthogonality', Warning)

        sy = np.sqrt(rot[0, 0] ** 2 + rot[1, 0] ** 2)

        singular = sy < 1e-6

        if not singular:
            x = np.arctan2(rot[2, 1], rot[2, 2])
            y = np.arctan2(-rot[2, 0], sy)
            z = np.arctan2(rot[1, 0], rot[0, 0])
        else:
            x = np.arctan2(-rot[1, 2], rot[1, 1])
            y = np.arctan2(-rot[2, 0], sy)
            z = 0

        return np.array([x, y, z])
    elif R.ndim == 3:
        euler = np.zeros((R.shape[0], 3))
        for k in range(R.shape[0]):
            rot = R[k, :, :]
            # Checks if a matrix is a valid rotation matrix. Transpose of rot should be inverse.
            # assert (linalg.norm(np.eye(3, dtype=rot.dtype) - rot.T @ rot) < 1e-6)
            if linalg.norm(np.eye(3, dtype=rot.dtype) - rot.T @ rot) > 1e-6:
                warnings.warn('Rotation matrix deviates from orthogonality', Warning)

            sy = np.sqrt(rot[0, 0] ** 2 + rot[1, 0] ** 2)

            singular = sy < 1e-6

            if not singular:
                x = np.arctan2(rot[2, 1], rot[2, 2])
                y = np.arctan2(-rot[2, 0], sy)
                z = np.arctan2(rot[1, 0], rot[0, 0])
            else:
                x = np.arctan2(-rot[1, 2], rot[1, 1])
                y = np.arctan2(-rot[2, 0], sy)
                z = 0
            euler[k, 0] = x
            euler[k, 1] = y
            euler[k, 2] = z
        return euler


def quat2rot(q: np.ndarray) -> np.ndarray:
    """Transforms input quaternion or array of quaternions to
    output rotation matrix or 3d array of rotation matrices.

    Parameters
    ----------
    q: np.ndarray
        Quaternion q = [q_r, q_i, q_j, q_k] or array of quaternions.

    Returns
    -------
    Rotation matrix
    """
    q = np.asarray(q)

    if len(q.shape) == 1:
        q = q[:, np.newaxis]

    # Normalize quaternion
    q /= np.sum(q*q, axis=0)

    rot = np.zeros((3, 3, q.shape[1]), dtype=q.dtype)
    # Row 1
    rot[0, 0, :] = q[0, :]**2 + q[1, :]**2 - q[2, :]**2 - q[3, :]**2
    rot[0, 1, :] = 2*(q[1, :]*q[2, :] - q[0, :]*q[3, :])
    rot[0, 2, :] = 2*(q[1, :]*q[3, :] + q[0, :]*q[2, :])
    # Row 2
    rot[1, 0, :] = 2*(q[1, :]*q[2, :] + q[0, :]*q[3, :])
    rot[1, 1, :] = q[0, :]**2 + q[2, :]**2 - q[1, :]**2 - q[3, :]**2
    rot[1, 2, :] = 2*(q[2, :]*q[3, :] - q[0, :]*q[1, :])
    # Row 3
    rot[2, 0, :] = 2*(q[1, :]*q[3, :] - q[0, :]*q[2, :])
    rot[2, 1, :] = 2*(q[2, :]*q[3, :] + q[0, :]*q[1, :])
    rot[2, 2, :] = q[0, :]**2 + q[3, :]**2 - q[1, :]**2 - q[2, :]**2
    return rot.squeeze()


def rot2quat(rot: np.ndarray) -> np.ndarray:
    """Convert array of rotation matrices to array of corresponding
    unit quaternions.
    If the input matrices are not orthonormal, the function will
    return the quaternions that correspond to the orthonormal matrices
    closest to the imprecise matrix inputs.

    Parameters
    ----------
    rot: :class:`.numpy.ndarray`
        3-by-3-by-N array of rotation matrices.

    Returns
    -------
        N-by-4 matrix containing N quaternions. Each quaternion is of the
        form q = [w x y z], with a scalar number as the first value.

    References
    ----------
    [1] I.Y. Bar-Itzhack, "New method for extracting the quaternion from a
      rotation matrix," Journal of Guidance, Control, and Dynamics,
      vol. 23, no. 6, pp. 1085-1087, 2000
    """
    if rot.size == 9:
        rot = rot[:, :, np.newaxis]

    # Pre-allocate output
    quat = np.zeros((4, rot.shape[2]), dtype=rot.dtype)

    # Calculate all elements of symmetric k_mat matrix
    k11 = rot[0, 0, :] - rot[1, 1, :] - rot[2, 2, :]
    k12 = rot[0, 1, :] + rot[1, 0, :]
    k13 = rot[0, 2, :] + rot[2, 0, :]
    k14 = rot[2, 1, :] - rot[1, 2, :]

    k22 = rot[1, 1, :] - rot[0, 0, :] - rot[2, 2, :]
    k23 = rot[1, 2, :] + rot[2, 1, :]
    k24 = rot[0, 2, :] - rot[2, 0, :]

    k33 = rot[2, 2, :] - rot[0, 0, :] - rot[1, 1, :]
    k34 = rot[1, 0, :] - rot[0, 1, :]

    k44 = rot[0, 0, :] + rot[1, 1, :] + rot[2, 2, :]

    # Construct k_mat matrix according to paper
    k_mat = np.array([[k11, k12, k13, k14],
                      [k12, k22, k23, k24],
                      [k13, k23, k33, k34],
                      [k14, k24, k34, k44]])

    k_mat /= 3

    # For each input rotation matrix, calculate the corresponding eigenvalues
    # and eigenvectors. The eigenvector corresponding to the largest eigenvalue
    # is the unit quaternion representing the same rotation.
    for s in range(rot.shape[2]):
        eig_values, eig_vectors = np.linalg.eigh(k_mat[:, :, s])

        max_id = np.argmax(eig_values)
        quat[:, s] = np.real(np.array([eig_vectors[3, max_id],
                                       eig_vectors[0, max_id],
                                       eig_vectors[1, max_id],
                                       eig_vectors[2, max_id]]))

        # By convention, always keep scalar quaternion element positive.
        # Note that this does not change the rotation that is represented
        # by the unit quaternion, since q and -q denote the same rotation.
        if quat[0, s] < 0:
            quat[:, s] = -quat[:, s]

    # # Normalize
    # quat /= np.sum(quat*quat, axis=0)

    return quat


def hat(x: np.ndarray) -> np.ndarray:
    """Cross product matrix:
        [[0, -x[2], x[1]],
        [x[2], 0, -x[0]],
        [-x[1], x[0], 0]]

    Parameters
    ----------
    x: np.ndarray
        3D vector or array of 3D vectors
    Returns
    -------
    3x3 hat matrix or array of screw matrices.
    """
    if len(x.shape) > 1:
        xx = np.zeros((3, 3) + x.shape[1:])
    else:
        xx = np.zeros((3, 3))
    # xx[0, 0, ...] = 0
    xx[1, 0, ...] = x[2, ...]
    xx[2, 0, ...] = -x[1, ...]

    xx[0, 1, ...] = -x[2, ...]
    # xx[1, 1, ...] = 0
    xx[2, 1, ...] = x[0, ...]

    xx[0, 2, ...] = x[1, ...]
    xx[1, 2, ...] = -x[0, ...]
    # xx[2, 2, ...] = 0
    return xx.squeeze()



#%% Show results
def show_result(r_i: ndarray,
                x_ik: Optional[ndarray] = None,
                R_k: Optional[ndarray] = None,
                t_k: Optional[ndarray] = None,
                w_i: Optional[ndarray] = None,
                threshold: Optional[float] = 0,
                length_ray: Optional[float] = 1.0,
                show_rays: Optional[bool] = True,
                show_reference: Optional[bool] = False,
                show_pose: Optional[bool] = False,
                show_origin: Optional[bool] = False,
                num_rays: Optional[int] = 500,
                figure_name: Optional[str] = None):
    """Displays r_i as 3d plot of rays or plots corresponding reference points.

    Args:
        r_i: Array of r_i r_i = (d_i, m_i).
        x_ik: 3D points corresponding to i-th ray and k-th pose.
        R_k: Initialization of rotation.
        t_k: Initialization of translation.
        w_i: Weight for each ray. Used to filter outliers.
        threshold: Threshold on weight factor to filter outliers.
        length_ray: Length of the plotted line symbolizing a ray (in meters).
        show_rays: Shows rays through reference points.
        show_reference: Shows reference point to corresponding ray.
        show_pose: Show monitor coordinate system.
        show_origin: Show closest point to all rays.
        num_rays: Number of r_i to be shown.
        figure_name: Name of figure. Plot result to figure_name figure.
    """

    fig = plt.figure(figure_name)
    plt.clf()
    Axes3D
    ax = fig.add_subplot(111, projection='3d')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')

    # Display random r_i
    if w_i is None:
        idx = np.random.permutation(r_i.shape[0])[0:num_rays]
    else:
        idx = np.random.permutation(np.argwhere(w_i > threshold))[0:num_rays, 0]
    if show_rays:
        # Calculate point on ray
        d_i, m_i = get_direction_moment_vector(r_i)
        principal_point = np.cross(d_i, m_i)

        # Get point on z==0
        t = -principal_point[:, 2]/(d_i[:, 2])
        start = principal_point + t[:, np.newaxis]*d_i

        ax.plot(start[idx, 0].T, start[idx, 1].T, start[idx, 2].T, 'r.')
        for idd in idx:
            ax.plot([start[idd, 0] - length_ray*d_i[idd, 0], start[idd, 0] + length_ray*d_i[idd, 0]],
                    [start[idd, 1] - length_ray*d_i[idd, 1], start[idd, 1] + length_ray*d_i[idd, 1]],
                    [start[idd, 2] - length_ray*d_i[idd, 2], start[idd, 2] + length_ray*d_i[idd, 2]],
                    'b-', linewidth=0.5, alpha=0.5)
        if show_origin:
            x_o = _calc_origin(r_i)
            ax.plot(x_o[0, np.newaxis], x_o[1, np.newaxis], x_o[2, np.newaxis], '.', markersize=5)

    if show_reference:
        for k in range(R_k.shape[0]):
            points = np.zeros((idx.shape[0], 3))
            for idd in range(idx.shape[0]):
                points[idd, :] = R_k[k, :, 0:2] @ x_ik[idx[idd], k, 0:2] + t_k[k, :, 0]
                if x_ik.shape[2] == 3:
                    points[idd, :] += R_k[k, :, 2]*x_ik[idx[idd], k, 2]

            # fig = plt.figure()
            # ax = fig.add_subplot(111, projection='3d')

            # ax.plot(points[0, :], points[1, :], points[2, :], linewidth=0.5)
            ax.plot(points[:, 0], points[:, 1], points[:, 2], '.', markersize=2, label=f'Pose {k}')

            # xx = xx*pixel_pitch*np.max(self.sensor_shape)
            # yy = yy*pixel_pitch*np.max(self.sensor_shape)

            # xx, yy = np.zeros((2, 2)), np.zeros((2, 2))
            # xx[:, 0] = np.min(x_ik[0, idx, k])*2
            # xx[:, 1] = np.max(x_ik[0, idx, k])/2
            # yy[0, :] = np.min(x_ik[1, idx, k])*2
            # yy[1, :] = np.max(x_ik[1, idx, k])/2
            #
            # x_point = (R_k[k, 0, 0]*xx.ravel() + R_k[k, 0, 1]*yy.ravel() + t_k[k, 0, 0]).reshape(2, 2)
            # y_point = (R_k[k, 1, 0]*xx.ravel() + R_k[k, 1, 1]*yy.ravel() + t_k[k, 1, 0]).reshape(2, 2)
            # z_point = (R_k[k, 2, 0]*xx.ravel() + R_k[k, 2, 1]*yy.ravel() + t_k[k, 2, 0]).reshape(2, 2)
            # ax.plot_surface(x_point, y_point, z_point, alpha=0.5)
            # ax.plot(x_point.ravel(), y_point.ravel(), z_point.ravel(), linewidth=0.5)
        # ax.legend()

    if show_pose:
        for k in range(R_k.shape[0]):
            end0 = R_k[k, :, 0]*50
            end1 = R_k[k, :, 1]*50
            end2 = R_k[k, :, 2]*50
            xk = np.asarray([np.min(x_ik[idx, k, 0]), np.min(x_ik[idx, k, 1]), 0])
            start = R_k[k, :, :]@xk + t_k[k, :, 0]
            ax.plot([start[0], start[0] + end0[0]],
                    [start[1], start[1] + end0[1]],
                    [start[2], start[2] + end0[2]], 'r-', linewidth=1, alpha=1)
            ax.plot([start[0], start[0] + end1[0]],
                    [start[1], start[1] + end1[1]],
                    [start[2], start[2] + end1[2]], 'g-', linewidth=1, alpha=1)
            ax.plot([start[0], start[0] + end2[0]],
                    [start[1], start[1] + end2[1]],
                    [start[2], start[2] + end2[2]], 'b-', linewidth=1, alpha=1)

    plt.draw()
    plt.pause(0.01)


def show_ray_parameters(r_i: ndarray,
                        mask: ndarray,
                        sensor_shape: tuple):
    """Shows ray parameters on pixel level in plücker coordinate configuration

    Args:
        r_i: Camera rays.
        mask: Valid pixels.
        sensor_shape: Shape of sensor in pixels.
    """

    plt.figure('Ray parameter')
    plt.subplot(3, 3, 1)
    plt.title('Direction')
    img = np.nan*np.zeros(sensor_shape, dtype=r_i.dtype)
    img[mask] = r_i[:, 0]
    plt.imshow(img)
    plt.subplot(3, 3, 4)
    img *= np.nan
    img[mask] = r_i[:, 1]
    plt.imshow(img)
    plt.subplot(3, 3, 7)
    img *= np.nan
    img[mask] = r_i[:, 2]
    plt.imshow(img)

    plt.subplot(3, 3, 2)
    plt.title('Moment')
    img *= np.nan
    img[mask] = r_i[:, 3]
    plt.imshow(img)
    plt.subplot(3, 3, 5)
    img *= np.nan
    img[mask] = r_i[:, 4]
    plt.imshow(img)
    plt.subplot(3, 3, 8)
    img *= np.nan
    img[mask] = r_i[:, 5]
    plt.imshow(img)
    plt.show()

    plt.subplot(3, 3, 3)
    plt.title('Starting point')
    # Get point on z==0
    t = -(r_i[:, 0] * r_i[:, 4] - r_i[:, 1] * r_i[:, 3]) / r_i[:, 2]
    img *= np.nan
    img[mask] = r_i[:, 1]*r_i[:, 5] - r_i[:, 2]*r_i[:, 4] + t*r_i[:, 0]
    plt.imshow(img)
    plt.subplot(3, 3, 6)
    img *= np.nan
    img[mask] = r_i[:, 2]*r_i[:, 3] - r_i[:, 0]*r_i[:, 5] + t*r_i[:, 1]
    plt.imshow(img)
    plt.subplot(3, 3, 9)
    img *= np.nan
    img[mask] = r_i[:, 0]*r_i[:, 4] - r_i[:, 1]*r_i[:, 3] + t*r_i[:, 2]
    plt.imshow(img)
    plt.show()


#%% Read and save data
def read_calibration_data(path: str,
                          file_name: str,
                          get_covariance: Optional[bool] = False,
                          max_num_files: Optional[int] = 1000,
                          percentage_pixel: Optional[float] = 100,
                          load_bad_pixel: Optional[bool] = False) \
        -> Tuple[ndarray, ndarray, ndarray, ndarray, ndarray, ndarray, ndarray]:
    """Reads calibration data from file.

    Args:
        path: Path to calibration data.
        file_name: Name of file. 'file_name' needs to be part of the true file name.
        get_covariance: get covariance data or not.
        max_num_files: Maximum number of monitor poses (camera views) used in calibration.
        percentage_pixel: Only calibrate a part of the camera rays, if their isn't enough RAM/VRAM.
        load_bad_pixel: Load worst pixels (highest uncertainty). Use only if percentage_pixel<1 and
                        when the poses are already calibrated. Use only 1 iteration for calibration!

    Returns:
        Returns x_ik, w_ik, C_ik, R_k, t_k, mask, sensor_shape
    """

    # get files
    os.scandir()
    file_list = os.listdir(os.path.abspath(path))
    file_list = [os.path.join(path, file) for file in file_list
                 # if file.lower().endswith('.npz') and file.lower().find(f'{file_name}') > -1]
                 if file.lower().find(f'{file_name}') > -1]
    # sort in numerical order
    file_list.sort(key=lambda key: [(lambda text: int(text) if text.isdigit() else text.lower())(c) for c in
                                    re.split('([0-9]+)', key)])

    num_poses = min(max_num_files, len(file_list))

    # Load calibration file
    calib_file = np.load(file_list[0])

    # get mask to avoid bad pixels
    # take p percentage of pixels
    p = percentage_pixel
    mask = np.sqrt(calib_file['uncertainty_xm'] ** 2 + calib_file['uncertainty_ym'] ** 2)
    if p < 100:
        mask[np.isinf(mask)] = 10
        mask[np.isnan(mask)] = 10
        hist, bin_edges = np.histogram(mask.ravel(), bins='auto', density=True)
        thresh_id = np.max(np.cumsum(np.cumsum(hist) < np.sum(hist)*p))
        mask = mask <= bin_edges[thresh_id]
    else:
        mask = np.ones_like(mask, dtype=np.bool)

    # load good or bad values?
    if (percentage_pixel < 100) and load_bad_pixel:
        mask = (1 - mask).astype(np.bool)
        print('Load bad pixel data')

    # x = int(mask.shape[0]/(4 + 2*math.sqrt(2)))
    # y = int(mask.shape[1]/(4 + 2*math.sqrt(2)))
    # mask[x:-x, y:-y] = True
    if np.sum(mask)/mask.size < 0.1:
        warnings.warn('Mask threshold to low?', Warning)
    if np.sum(mask)/mask.size > 0.8:
        warnings.warn('Mask threshold to high? Check available memory!', Warning)

    # Camera parameters
    sensor_shape = calib_file['registration_xm'].shape

    # Transform pixel to meter
    pixel_pitch = calib_file['monitor_pixel_pitch']

    # Monitor parameters
    # Load monitor points data and uncertainty data
    num_rays = calib_file['registration_xm'][mask].size
    x_ik = np.zeros((num_rays, num_poses, 2), dtype=np.float32)
    w_ik = np.zeros((num_rays, num_poses), dtype=np.float32)

    if get_covariance:
        C_ik = np.zeros((num_rays, num_poses, 2), dtype=np.float32)
    else:
        C_ik = np.zeros((0, 0, 2), dtype=np.float32)

    # Pose parameters
    R_k = np.zeros((num_poses, 3, 3), dtype=np.float32)
    t_k = np.zeros((num_poses, 3, 1), dtype=np.float32)

    # Load parameters
    for k in range(0, num_poses):
        print(f'Load: {file_list[k]}')
        calib_file = np.load(file_list[k])

        # Monitor points
        # Transform pixel to meter
        x_ik[:, k, 0] = (calib_file['registration_xm'][mask].ravel())*pixel_pitch
        x_ik[:, k, 1] = (calib_file['registration_ym'][mask].ravel())*pixel_pitch

        # Uncertainties sqrt(Cxx)
        if get_covariance:
            C_ik[:, k, 0] = (calib_file['uncertainty_xm'][mask].ravel())
            C_ik[:, k, 1] = (calib_file['uncertainty_ym'][mask].ravel())

        # Calculate single weight factor
        w_ik[:, k] = 2.0/(calib_file['uncertainty_xm'][mask].ravel()**2 +
                          calib_file['uncertainty_ym'][mask].ravel()**2 + 1e-18)

        # Pose: try to load an initialization
        try:
            R_k[k, :, :] = calib_file['monitor_rotation']
        except KeyError:
            R_k[k, :, :] = np.eye(3)
        try:
            t_k[k, :, 0] = calib_file['monitor_translation'].squeeze()
        except KeyError:
            # camera x,y-coordinates are approximately chosen to correspond
            # to weighted mean of observed monitor coordinates
            # t_k[k, 0, 0] = np.mean(x_ik[:, k, 0])
            # t_k[k, 1, 0] = np.mean(x_ik[:, k, 1])
            t_k[k, 0, 0] = -np.mean(x_ik[:, k, 0]*w_ik[:, k])/np.mean(w_ik[:, k])
            t_k[k, 1, 0] = -np.mean(x_ik[:, k, 1]*w_ik[:, k])/np.mean(w_ik[:, k])
            # approximately estimate depth (define each depth or use an increasing depth
            # or use weighted variance or TODO: whatever...)
            t_k[k, 2, 0] = k*0.05 + 0.05
            # t_k[k, 2, 0] = np.sqrt(np.var(x_ik[:, k, 0]*w_ik[:, k]) +
            #                        np.var(x_ik[:, k, 1]*w_ik[:, k]))/np.mean(w_ik[:, k]**2)

    # t_k[:, 2, 0] = (t_k[:, 2, 0] - t_k[:, 2, 0].min())/(t_k[:, 2, 0].max() - t_k[:, 2, 0].min())

    # # convert units? m to mm
    # x_ik *= 1000
    # t_k *= 1000

    # load good or bad values?
    if (percentage_pixel < 100) and load_bad_pixel:
        # TODO: that's ugly...
        calib_file = np.load(path + 'temporary_calibration_result.npz')
        R_k = calib_file['R_k']
        t_k = calib_file['t_k']
        print('Load previously calculated monitor poses!')

    # fix nan values
    w_ik[np.any(np.isnan(x_ik), axis=2)] = 0
    x_ik[np.isnan(x_ik)] = 0
    w_ik[np.isnan(w_ik)] = 0

    # scale uncertainty for numerical stability (only if all pixels are used)
    if percentage_pixel == 100:
        w_ik /= np.mean(w_ik)
        # C_ik /= np.mean(C_ik)

    return x_ik, w_ik, C_ik, R_k, t_k, mask, sensor_shape
