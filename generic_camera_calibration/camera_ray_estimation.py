# Copyright (C) 2021  David Uhlig
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from math import sqrt, fabs, isnan
from typing import Optional

import numpy as np
import numpy.linalg as linalg
from numba import njit, prange, cuda, float64
from numba.cuda import devicearray
from numpy.core.multiarray import ndarray


#%% wrapper function
def estimate(r_i: ndarray,
             x_ik: ndarray,
             w_ik: ndarray,
             R_k: ndarray,
             t_k: ndarray,
             method: Optional[str] = 'p2l_edist') -> None:
    """Calls ray calibration based on specified method.
    Input array r_i is modified according to calibration.

    Args:
        r_i: Camera rays.
        x_ik: observed points in local coordinates.
        w_ik: Weight factor.
        R_k: Rotation matrix.
        t_k: Translation vector.
        method: Calibration method.
    """
    if method.lower() == 'p2l_edist':
        calibration_p2l_edist(r_i, x_ik, w_ik, R_k, t_k)
    elif method.lower() == 'p2l_edist_cuda':
        # get CUDA parameters
        device = cuda.get_current_device()
        tpb = device.WARP_SIZE
        n = x_ik.shape[0]
        bpg = int(np.ceil(float(n)/tpb))
        calibration_p2l_edist_cuda[bpg, tpb](r_i, x_ik, w_ik, R_k, t_k)
    else:
        raise NotImplementedError(f'Method "{method}" not implemented.')


#%% cpu implementation
@njit
def calibration_p2l_edist(r_i: ndarray,
                          x_ik: ndarray,
                          w_ik: ndarray,
                          R_k: ndarray,
                          t_k: ndarray):
    """Estimates for every pixel the ray direction and momentum.
    Minimizes the least squares error of the (weighted) euclidean point to line distance.

    Args:
        r_i: (i,6)-array of rays r_i = (d_i, m_i), which will be estimated
            or (i,4)-array of rays r_i = (d_i[0:2], m_i[0:2]).
        x_ik: 3D points corresponding to i-th ray and k-th pose as (3,i,k)-array.
        w_ik: Weight factor of each point.
        R_k: Estimate of k rotations in (3,3,k)-array.
        t_k: Estimate of k translations in(3,1,k)-array.
    """

    for i in prange(r_i.shape[0]):
        # weighted centroid
        p_i = np.zeros(3, dtype=np.float64)

        # mean weight
        wi = 0

        A1 = np.zeros((3, 3), dtype=np.float64)
        for k in range(x_ik.shape[1]):
            # Transform point coordinates
            p_ik = R_k[k, 0:3, 0:2]@x_ik[i, k, 0:2] + t_k[k, :, 0]
            if x_ik.shape[2] == 3:
                p_ik += R_k[k, :, 2]*x_ik[i, k, 2]

            # Calc screw matrix Px_ik = [p_ik]x
            Px_ik = np.zeros((3, 3), dtype=np.float64)
            Px_ik[1, 0] = p_ik[2]
            Px_ik[2, 0] = -p_ik[1]
            Px_ik[0, 1] = -p_ik[2]
            Px_ik[2, 1] = p_ik[0]
            Px_ik[0, 2] = p_ik[1]
            Px_ik[1, 2] = -p_ik[0]

            A1 += w_ik[i, k]*Px_ik.T@Px_ik
            p_i += w_ik[i, k]*p_ik
            wi += w_ik[i, k]

        # Calc screw matrix Px_i = [p_i]x
        Px_i = np.zeros((3, 3), dtype=np.float64)
        Px_i[1, 0] = p_i[2]
        Px_i[2, 0] = -p_i[1]
        Px_i[0, 1] = -p_i[2]
        Px_i[2, 1] = p_i[0]
        Px_i[0, 2] = p_i[1]
        Px_i[1, 2] = -p_i[0]

        wi = (1.0/wi)
        P = A1 - wi*Px_i.T@Px_i

        # Calculate eigenvalues of matrix P
        eig_values, eig_vectors = linalg.eigh(P)

        # Get eigenvector to smallest eigenvalue
        d_i = eig_vectors[:, np.argmin(np.abs(eig_values))]

        # define d_i[2] to be positive (useful for 4-parameter ray representation)
        if d_i[2] < 0:
            d_i *= -1

        # get ray moment
        m_i = wi*Px_i@d_i

        if r_i.shape[1] == 6:
            r_i[i, 0:3] = d_i
            r_i[i, 3:6] = m_i
        elif r_i.shape[1] == 4:
            # d3 and m3 can be calculated from the 4 remaining parameters
            r_i[i, 0:2] = d_i[0:2]
            r_i[i, 2:4] = m_i[0:2]

        # set nan vectors to zero
        if np.any(np.isnan(r_i[i, :])):
            r_i[i, :] = 0



#%% cuda implementation in separated file, because too much symbolic equations......
@cuda.jit
def calibration_p2l_edist_cuda(r_i: devicearray,
                               x_ik: devicearray,
                               w_ik: devicearray,
                               R_k: devicearray,
                               t_k: devicearray):
    """Estimates for every pixel the ray direction and momentum.
    Minimizes the least squares error of the (weighted) euclidean point to line distance.

    Args:
        r_i: (i,6)-array of rays r_i = (d_i, m_i), which will be estimated
             or (i,4)-array of rays r_i = (d_i[0:2], m_i[0:2]).
        x_ik: 3D points corresponding to i-th ray and k-th pose as (3,i,k)-array.
        w_ik: Weight factor of each point.
        R_k: Estimate of k rotations in (3,3,k)-array.
        t_k: Estimate of k translations in(3,1,k)-array.
    """

    i = cuda.grid(1)
    if i < r_i.shape[0]:
        # initialize matrices
        Add = cuda.local.array((3,3), dtype=float64)
        Amd = cuda.local.array((3,3), dtype=float64)
        Amm = cuda.local.array(1, dtype=float64)

        Add[0, 0] = Add[1, 0] = Add[2, 0] = 0
        Add[0, 1] = Add[1, 1] = Add[2, 1] = 0
        Add[0, 2] = Add[1, 2] = Add[2, 2] = 0
        Amd[0, 0] = Amd[1, 0] = Amd[2, 0] = 0
        Amd[0, 1] = Amd[1, 1] = Amd[2, 1] = 0
        Amd[0, 2] = Amd[1, 2] = Amd[2, 2] = 0
        Amm[0] = 0

        # calculate objective function matrices:
        for k in range(x_ik.shape[1]):
            wik = w_ik[i, k]

            # exclude bad pixels
            if wik > 0.0:
                # Transform point coordinates p_ik = R_k@x_ik + t_k
                p_ik = cuda.local.array(3, dtype=float64)
                p_ik[0] = R_k[k, 0, 0]*x_ik[i, k, 0] + R_k[k, 0, 1]*x_ik[i, k, 1] + t_k[k, 0, 0]
                p_ik[1] = R_k[k, 1, 0]*x_ik[i, k, 0] + R_k[k, 1, 1]*x_ik[i, k, 1] + t_k[k, 1, 0]
                p_ik[2] = R_k[k, 2, 0]*x_ik[i, k, 0] + R_k[k, 2, 1]*x_ik[i, k, 1] + t_k[k, 2, 0]
                if x_ik.shape[2] == 3:
                    p_ik[0] += R_k[k, 0, 2]*x_ik[i, k, 2]
                    p_ik[1] += R_k[k, 1, 2]*x_ik[i, k, 2]
                    p_ik[2] += R_k[k, 2, 2]*x_ik[i, k, 2]

                Add[0, 0] += wik*(p_ik[1]**2 + p_ik[2]**2)
                Add[1, 0] += -wik*p_ik[0]*p_ik[1]
                Add[2, 0] += -wik*p_ik[0]*p_ik[2]
                Add[0, 1] += -wik*p_ik[0]*p_ik[1]
                Add[1, 1] += wik*(p_ik[0]**2 + p_ik[2]**2)
                Add[2, 1] += -wik*p_ik[1]*p_ik[2]
                Add[0, 2] += -wik*p_ik[0]*p_ik[2]
                Add[1, 2] += -wik*p_ik[1]*p_ik[2]
                Add[2, 2] += wik*(p_ik[0]**2 + p_ik[1]**2)

                # Amd[0, 0] += 0
                Amd[1, 0] += -2*p_ik[2]*wik
                Amd[2, 0] += 2*p_ik[1]*wik
                Amd[0, 1] += 2*p_ik[2]*wik
                # Amd[1, 1] += 0
                Amd[2, 1] += -2*p_ik[0]*wik
                Amd[0, 2] += -2*p_ik[1]*wik
                Amd[1, 2] += 2*p_ik[0]*wik
                # Amd[2, 2] += 0

                Amm[0] += wik

        # invert for later reuse
        Amm[0] = 1.0/Amm[0]

        # get   P= Add -  0.25*Amd.T@Amd
        # reuse P==Add -= 0.25*Amd.T@Amd
        # P = cuda.local.array((3,3), dtype=float64)
        Add[0, 0] += -Amm[0]/4*(Amd[0, 1]**2 + Amd[0, 2]**2)
        Add[0, 1] += -Amm[0]/4*(Amd[0, 2]*Amd[1, 2])
        Add[0, 2] += -Amm[0]/4*(Amd[0, 1]*Amd[2, 1])
        Add[1, 0] += -Amm[0]/4*(Amd[0, 2]*Amd[1, 2])
        Add[1, 1] += -Amm[0]/4*(Amd[1, 0]**2 + Amd[1, 2]**2)
        Add[1, 2] += -Amm[0]/4*(Amd[1, 0]*Amd[2, 0])
        Add[2, 0] += -Amm[0]/4*(Amd[0, 1]*Amd[2, 1])
        Add[2, 1] += -Amm[0]/4*(Amd[1, 0]*Amd[2, 0])
        Add[2, 2] += -Amm[0]/4*(Amd[2, 0]**2 + Amd[2, 1]**2)

        # calculate eigenvalues of symmetric matrix Add
        eig_val = cuda.local.array(3, dtype=float64)
        eig_vec = cuda.local.array((3, 3), dtype=float64)
        eigh(Add, eig_vec, eig_val)
        eig_val[0] = abs(eig_val[0])
        eig_val[1] = abs(eig_val[1])
        eig_val[2] = abs(eig_val[2])

        # get ray direction
        if (eig_val[0] < eig_val[1]) and (eig_val[0] < eig_val[2]):
            d_i_0 = eig_vec[0, 0]
            d_i_1 = eig_vec[1, 0]
            d_i_2 = eig_vec[2, 0]
        elif (eig_val[1] < eig_val[0]) and (eig_val[1] < eig_val[2]):
            d_i_0 = eig_vec[0, 1]
            d_i_1 = eig_vec[1, 1]
            d_i_2 = eig_vec[2, 1]
        else:  # (eig_val[2] < eig_val[0]) and (eig_val[2] < eig_val[1]):
            d_i_0 = eig_vec[0, 2]
            d_i_1 = eig_vec[1, 2]
            d_i_2 = eig_vec[2, 2]

        # TODO: is this already normalized to ||d|| == 1?
        d_norm = sqrt(d_i_0**2 + d_i_1**2 + d_i_2**2)
        d_i_0 /= d_norm
        d_i_1 /= d_norm
        d_i_2 /= d_norm

        # define d_i[2] to be positive (useful for 4-parameter ray representation)
        if d_i_2 < 0:
            d_i_0 *= -1
            d_i_1 *= -1
            d_i_2 *= -1

        if r_i.shape[1] == 6:
            r_i[i, 0] = d_i_0
            r_i[i, 1] = d_i_1
            r_i[i, 2] = d_i_2
            # get ray moment
            # r_i[i, 3:6] = -0.5/Amm*Amd@r_i[i, 0:3]
            r_i[i, 3] = -0.5*Amm[0]*(Amd[0, 1]*d_i_1 + Amd[0, 2]*d_i_2)
            r_i[i, 4] = -0.5*Amm[0]*(Amd[1, 0]*d_i_0 + Amd[1, 2]*d_i_2)
            r_i[i, 5] = -0.5*Amm[0]*(Amd[2, 0]*d_i_0 + Amd[2, 1]*d_i_1)
            # set ray to zero if error occurred
            if isnan(r_i[i, 0]) or isnan(r_i[i, 1]) or isnan(r_i[i, 2]) \
                    or isnan(r_i[i, 3]) or isnan(r_i[i, 4]) or isnan(r_i[i, 5]):
                r_i[i, 0] = r_i[i, 1] = r_i[i, 2] = 0
                r_i[i, 3] = r_i[i, 4] = r_i[i, 5] = 0
        elif r_i.shape[1] == 4:
            # d3 and m3 can be calculated from the 4 remaining parameters
            r_i[i, 0] = d_i_0
            r_i[i, 1] = d_i_1
            r_i[i, 2] = -0.5*Amm[0]*(Amd[0, 1]*d_i_1 + Amd[0, 2]*d_i_2)
            r_i[i, 3] = -0.5*Amm[0]*(Amd[1, 0]*d_i_0 + Amd[1, 2]*d_i_2)
            if isnan(r_i[i, 0]) or isnan(r_i[i, 1]) or isnan(r_i[i, 2]) or isnan(r_i[i, 3]):
                r_i[i, 0] = r_i[i, 1] = r_i[i, 2] = r_i[i, 3] = 0


#%% eigen decomposition of symmetric 3x3 Matrix
@cuda.jit(device=True)
def eigh(A, Q, w):
    """Calculates the eigenvalues and normalized eigenvectors of a symmetric 3x3
    matrix A using the QL algorithm with implicit shifts, preceded by a
    Householder reduction to tridiagonal form.
    The function accesses only the diagonal and upper triangular parts of A.
    The access is read-only.

    Args:
        A: The symmetric input matrix.
        Q: Storage buffer for eigenvectors.
        w: Storage buffer for eigenvalues.

    Returns:
        0 Success.
        -1 Error (no convergence).
    """

    n = 3
    e = cuda.local.array(n, dtype=float64)  # The third element is used only as temporary workspace
    e[0] = e[1] = e[2] = 0
    # e = np.zeros(3)

    # Transform A to real tridiagonal form by the Householder method
    tri_diag(A, Q, w, e)

    # Calculate eigensystem of the remaining real symmetric tridiagonal matrix
    # with the QL method

    # Loop over all off-diagonal elements
    for l in range(n - 1):
        nIter = 0
        while True:
            # Check for convergence and exit iteration loop if off-diagonal
            # element e(l) is zero
            m = l
            for m_ in range(l, n - 1):
                g = fabs(w[m]) + fabs(w[m + 1])
                if fabs(e[m]) + g == g:
                    break
                m += 1
            if m == l:
                break

            if nIter >= 30:
                return -1
            nIter += 1

            # Calculate g = d_m - k
            g = (w[l + 1] - w[l])/(e[l] + e[l])
            r = sqrt(g**2 + 1.0)
            if g > 0:
                g = w[m] - w[l] + e[l]/(g + r)
            else:
                g = w[m] - w[l] + e[l]/(g - r)

            s = c = 1.0
            p = 0.0
            for i in range(m - 1, l - 1, -1):
                f = s*e[i]
                b = c*e[i]
                if fabs(f) > fabs(g):
                    c = g/f
                    r = sqrt(c**2 + 1.0)
                    e[i + 1] = f*r
                    s = 1.0/r
                    c *= s
                else:
                    s = f/g
                    r = sqrt(s**2 + 1.0)
                    e[i + 1] = g*r
                    c = 1.0/r
                    s *= c
                g = w[i + 1] - p
                r = (w[i] - g)*s + 2.0*c*b
                p = s*r
                w[i + 1] = g + p
                g = c*r - b

                # Form eigenvectors
                # if Q is not None:
                for k in range(n):
                    t = Q[k][i + 1]
                    Q[k][i + 1] = s*Q[k][i] + c*t
                    Q[k][i] = c*Q[k][i] - s*t
            w[l] -= p
            e[l] = g
            e[m] = 0.0

    # Sort eigenvalues and corresponding vectors
    for i in range(n - 1):
        k = i
        p = w[i]
        for j in range(i + 1, n):
            if w[j] < p:
                k = j
                p = w[j]
        if k != i:
            w[k] = w[i]
            w[i] = p
            for j in range(n):
                p = Q[j][i]
                Q[j][i] = Q[j][k]
                Q[j][k] = p


@cuda.jit(device=True)
def tri_diag(A, Q, d, e) -> None:
    """
    Reduces a symmetric 3x3 matrix to tridiagonal form by applying
    (unitary) Householder transformations:
                [ d[0]  e[0]       ]
        A = Q @ [ e[0]  d[1]  e[1] ] @ Q.T
                [       e[1]  d[2] ]
    The function accesses only the diagonal and upper triangular parts of
    A. The access is read-only.

    Used in eigh() function.

    Args:
        A
        Q
        d
        e
    """

    n = 3
    u = cuda.local.array(n, dtype=float64)
    q = cuda.local.array(n, dtype=float64)
    u[0] = u[1] = u[2] = 0
    q[0] = q[1] = q[2] = 0
    # u = np.zeros(3)
    # q = np.zeros(3)

    # Initialize Q to the identitity matrix
    # if Q is not None:
    for i in range(n):
        Q[i][i] = 1.0
        for j in range(i):
            Q[i][j] = Q[j][i] = 0.0

    # Bring first row and column to the desired form
    h = A[0][1]**2 + A[0][2]**2
    if A[0][1] > 0:
        g = -sqrt(h)
    else:
        g = sqrt(h)
    e[0] = g
    f = g*A[0][1]
    u[1] = A[0][1] - g
    u[2] = A[0][2]

    omega = h - f
    if omega > 0.0:
        omega = 1.0/omega
        K = 0.0
        for i in range(n):
            f = A[1][i]*u[1] + A[i][2]*u[2]
            q[i] = omega*f  # p
            K += u[i]*f  # u* A u
        K *= 0.5*omega**2
        for i in range(n):
            q[i] = q[i] - K*u[i]

        d[0] = A[0][0]
        d[1] = A[1][1] - 2.0*q[1]*u[1]
        d[2] = A[2][2] - 2.0*q[2]*u[2]

        # Store inverse Householder transformation in Q
        # if Q is not None:
        for j in range(n):
            f = omega*u[j]
            for i in range(n):
                Q[i][j] = Q[i][j] - f*u[i]

        # Calculate updated A[1][2] and store it in e[1]
        e[1] = A[1][2] - q[1]*u[2] - u[1]*q[2]
    else:
        for i in range(3):
            d[i] = A[i][i]
        e[1] = A[1][2]


