# Copyright (C) 2021  David Uhlig
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from typing import Optional, Tuple

import numpy as np
import numpy.linalg as linalg
from numba import njit, prange, cuda
from numpy.core.multiarray import ndarray
from tqdm import tqdm

from generic_camera_calibration import calibration_utilities as utilities


# %%
def estimate(r_i: ndarray,
             x_ik: ndarray,
             w_ik: ndarray,
             R_k: ndarray,
             t_k: ndarray,
             method: str,
             is_pose_reflected: Optional[bool] = False) -> ndarray:
    """Main method to start pose estimation.

    Args:
        r_i: Array of r_i r_i = (d_i, m_i).
        x_ik: 3D points corresponding to i-th ray and k-th pose.
        w_ik: Corresponding weight factor.
        R_k: Initialization of rotation.
        t_k: Initialization of translation.
        method: Choose pose estimation
                method: icp_svd, icp_foam, least_squares, nonlin_least_squares
        is_pose_reflected: Search pose in the O(3)\SO(3) manifold that contains
                           reflected rotations. This results in det(R) = -1.

    Returns:
        Returns pose error for each pose.
    """
    method = method.lower()
    if method == 'least_squares':
        error = pose_least_squares(r_i, x_ik, w_ik, R_k, t_k, is_pose_reflected)
    else:
        raise NotImplementedError(f'Method \'{method}\' not implemented.')
    return error


#%% Direct pose calibration
def pose_least_squares(r_i: ndarray,
                       x_ik: ndarray,
                       w_ik: ndarray,
                       R_k: ndarray,
                       t_k: ndarray,
                       is_pose_reflected: Optional[bool] = False):
    """
    Finds rotation & translation that transforms point cloud x_ik,
    so that they have minimal distance to corresponding rays.
    Closed form solution based on least squares estimation.

    Args:
        r_i: Array of r_i r_i = (d_i, m_i).
        x_ik: 3D points corresponding to i-th ray and k-th pose.
        w_ik: Corresponding weight factor.
        R_k: Initialization of rotation.
        t_k: Initialization of translation.
        is_pose_reflected: Search pose in the O(3)\SO(3) manifold that contains
                           reflected rotations. This results in det(R) = -1.

    Returns:
        Returns pose error for all poses.
    """

    error = np.zeros(R_k.shape[0])
    for k in range(R_k.shape[0]):
        # calc quadratic functional
        if not cuda.is_cuda_array(x_ik):
            Arr, Atr, Att, br, bt, c = get_quadratic_functional(x_ik[:, k, :], r_i, w_ik[:, k])
        else:
            Arr = np.zeros(81, dtype=r_i.dtype)
            Atr = np.zeros(27, dtype=r_i.dtype)
            Att = np.zeros(9, dtype=r_i.dtype)
            br = np.zeros(9, dtype=r_i.dtype)
            bt = np.zeros(3, dtype=r_i.dtype)
            c = np.zeros(1, dtype=r_i.dtype)

            tpb = cuda.get_current_device().WARP_SIZE
            bpg = int(np.ceil(float(x_ik.shape[0])/tpb))
            get_quadratic_functional_cuda[bpg, tpb](x_ik, r_i, w_ik, k, Arr, Atr, Att, br, bt, c)
            Arr = Arr.reshape((9, 9), order='F')
            Atr = Atr.reshape((3, 9), order='F')
            Att = Att.reshape((3, 3), order='F')

        # matrices need to be symmetric (reduce numerical errors)
        Arr = 0.5*(Arr + Arr.T)
        Att = 0.5*(Att + Att.T)

        use_SE3 = False
        if use_SE3:
            # Refine pose using nonlinear optimization on SO(3)xR3 manifold
            R, t = refine_pose_SO3xR3(R_k[k, :, :], t_k[k, :, 0], Arr, Att, Atr, br, bt, c[0], verbose=0)
            # R, t = refine_pose_SE3(R_k[k, :, :], t_k[k, :, 0], Arr, Att, Atr, br, bt, c[0], verbose=0)
        else:  # use_SO3
            # get objective function for rotation
            # quadratic functional r.T@A_k@r + b_k.T@r + c_k --> min , with r = vec(R)
            try:
                temp1 = linalg.solve(Att, Atr)  # np.linalg.inv(Att)@Atr
                temp2 = linalg.solve(Att, bt)  # np.linalg.inv(Att)@bt
            except linalg.LinAlgError:
                temp1 = linalg.lstsq(Att, Atr, rcond=None)[0]  # np.linalg.inv(Att)@Atr
                temp2 = linalg.lstsq(Att, bt, rcond=None)[0]
            A_k = Arr - 0.25*Atr.T@temp1
            b_k = br - 0.5*temp1.T@bt
            c_k = (c[0] - 0.25*temp2.T@bt)

            # initialize with euclidean solution when there is no previous estimate
            if R_k is None:
                # Get euclidean least squares solution
                r = -0.5*linalg.pinv(A_k)@b_k
                # r = -0.5*linalg.lstsq(A_k, b_k, rcond=None)[0]

                # Get initial solution for rotation
                R = np.zeros((3, 3))
                R[:, 0] = r[0:3].squeeze()
                R[:, 1] = r[3:6].squeeze()
                R[:, 2] = r[6:9].squeeze()

                # Find closest rotation matrix --> Project to SO(3)
                U, _, Vh = linalg.svd(R)
                R_k[k, :, :] = U@np.diag([1, 1, linalg.det(Vh@U)])@Vh

            # Refine pose using nonlinear optimization on  SO(3) manifold
            R = refine_pose_SO3(R_k[k, :, :], A_k, 0.5*b_k, c_k, verbose=0, is_pose_reflected=is_pose_reflected)
            r = R.ravel(order="F")

            # TODO: check if the reference display points towards the camera?

            # Get least squares solution for translation
            try:
                t = -0.5*linalg.solve(Att, Atr@r + bt)  # np.linalg.inv(Att)@(...)
            except linalg.LinAlgError:
                t = -0.5*linalg.lstsq(Att, Atr@r + bt, rcond=None)[0]

        # update pose
        R_k[k, :, :] = R.astype(x_ik.dtype)
        t_k[k, :, 0] = t.astype(x_ik.dtype)

        # get function value
        r = R_k[k, :, :].ravel(order="F")
        error[k] = (r.T@Arr@r + t.T@Att@t + t.T@Atr@r + br.T@r + bt.T@t + c[0])/r_i.shape[0]
    return error


#%% Numba utilities
@njit
def get_quadratic_functional(x_i: ndarray,
                             r_i: ndarray,
                             w_i: ndarray) -> Tuple[ndarray, ndarray, ndarray, ndarray, ndarray, ndarray]:
    """Calculate quadratic functional from sum of rays and points.

    Args:
        x_i: Observed points.
        r_i: Camera rays.
        w_i: Weight factors.

    Returns:
        Matrices and vectors of quadratic functional.
    """

    # Initialize variables
    Arr = np.zeros((9, 9), dtype=r_i.dtype)
    Atr = np.zeros((3, 9), dtype=r_i.dtype)
    Att = np.zeros((3, 3), dtype=r_i.dtype)
    br = np.zeros(9, dtype=r_i.dtype)
    bt = np.zeros(3, dtype=r_i.dtype)
    c = np.zeros(1, dtype=r_i.dtype)

    for i in prange(r_i.shape[0]):

        # get ray parameter
        d_i, m_i = utilities.get_direction_moment_vector(r_i[i, :])

        # Calc screw matrix [d_i]x_i = Di
        Di = np.zeros((3, 3), dtype=r_i.dtype)
        Di[1, 0] = d_i[2]
        Di[2, 0] = -d_i[1]
        Di[0, 1] = -d_i[2]
        Di[2, 1] = d_i[0]
        Di[0, 2] = d_i[1]
        Di[1, 2] = -d_i[0]

        # calc kronecker product kron(xi.T, Di.T)
        xiT_DiT = np.zeros((3, 9), dtype=r_i.dtype)
        xiT_DiT[:, 0:3] = x_i[i, 0]*Di.T
        xiT_DiT[:, 3:6] = x_i[i, 1]*Di.T
        if x_i.shape[1] == 3:
            xiT_DiT[:, 6:9] = x_i[i, 2]*Di.T

        Arr += w_i[i]*xiT_DiT.T@xiT_DiT
        Atr += 2*w_i[i]*Di@xiT_DiT
        Att += w_i[i]*Di@Di.T
        br[:] += -2*w_i[i]*xiT_DiT.T@m_i
        bt[:] += -2*w_i[i]*Di@m_i
        c += w_i[i]*m_i.T@m_i

    return Arr, Atr, Att, br, bt, c


@cuda.jit
def get_quadratic_functional_cuda(x_ik: ndarray,
                                  r_i: ndarray,
                                  w_ik: ndarray,
                                  k: int,
                                  Arr: ndarray,
                                  Atr: ndarray,
                                  Att: ndarray,
                                  br: ndarray,
                                  bt: ndarray,
                                  c: ndarray):
    """Calculate quadratic functional from sum of rays and points.

    Args:
        x_ik: 3D points corresponding to i-th ray and k-th pose.
        r_i: Array of r_i r_i = (d_i, m_i).
        w_ik: Corresponding weight factor.
        k: Pose index.
        Arr: Matrix of quadratic functional.
        Atr: Matrix of quadratic functional.
        Att: Matrix of quadratic functional.
        br: Vector of quadratic functional.
        bt: Vector of quadratic functional.
        c:  Scalar of quadratic functional.

    Returns:
        Matrices and vectors of quadratic functional.
    """

    i = cuda.grid(1)
    if i < r_i.shape[0]:
        # get ray parameter
        d_i, m_i = utilities.get_direction_moment_vectors_cuda(r_i[i, :])

        if w_ik[i, k] > 0.01:  # hard threshold to filter outliers... TODO: estimate from w_ik[i, k] data!?!
            # ugly code from symbolic toolbox...
            if x_ik.shape[2] == 3:
                t2 = x_ik[i, k, 0]**2
                t3 = d_i[1]**2
                t4 = d_i[2]**2
                t5 = t2*t4*w_ik[i, k]
                t6 = d_i[0]**2
                t7 = t4*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1]
                t8 = t4*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 2]
                t9 = t2*t6*w_ik[i, k]
                t10 = t2*t3*w_ik[i, k]
                t11 = t6*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1]
                t12 = t3*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1]
                t13 = t6*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 2]
                t14 = t3*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 2]
                t15 = t7 + t12
                t16 = x_ik[i, k, 1]**2
                t17 = t7 + t11
                t18 = t4*t16*w_ik[i, k]
                t19 = t4*w_ik[i, k]*x_ik[i, k, 1]*x_ik[i, k, 2]
                t20 = t11 + t12
                t21 = t6*t16*w_ik[i, k]
                t22 = t3*t16*w_ik[i, k]
                t23 = t6*w_ik[i, k]*x_ik[i, k, 1]*x_ik[i, k, 2]
                t24 = t3*w_ik[i, k]*x_ik[i, k, 1]*x_ik[i, k, 2]
                t25 = t8 + t14
                t26 = t19 + t24
                t27 = x_ik[i, k, 2]**2
                t28 = t8 + t13
                t29 = t19 + t23
                t30 = t4*t27*w_ik[i, k]
                t31 = t13 + t14
                t32 = t23 + t24
                t33 = t6*t27*w_ik[i, k]
                t34 = t3*t27*w_ik[i, k]

                # %% summation over Arr
                idx = 0
                cuda.atomic.add(Arr, idx, t5 + t10)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[1]*t2*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[2]*t2*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, t15)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[1]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1])
                idx += 1
                cuda.atomic.add(Arr, idx, t25)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[1]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 2])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 2])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[1]*t2*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, t5 + t9)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[1]*d_i[2]*t2*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[1]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1])
                idx += 1
                cuda.atomic.add(Arr, idx, t17)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[1]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[1]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 2])
                idx += 1
                cuda.atomic.add(Arr, idx, t28)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[1]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 2])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[2]*t2*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[1]*d_i[2]*t2*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, t9 + t10)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[1]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1])
                idx += 1
                cuda.atomic.add(Arr, idx, t20)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 2])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[1]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 2])
                idx += 1
                cuda.atomic.add(Arr, idx, t31)
                idx += 1
                cuda.atomic.add(Arr, idx, t15)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[1]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1])
                idx += 1
                cuda.atomic.add(Arr, idx, t18 + t22)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[1]*t16*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[2]*t16*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, t26)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[1]*w_ik[i, k]*x_ik[i, k, 1]*x_ik[i, k, 2])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[2]*w_ik[i, k]*x_ik[i, k, 1]*x_ik[i, k, 2])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[1]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1])
                idx += 1
                cuda.atomic.add(Arr, idx, t17)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[1]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[1]*t16*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, t18 + t21)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[1]*d_i[2]*t16*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[1]*w_ik[i, k]*x_ik[i, k, 1]*x_ik[i, k, 2])
                idx += 1
                cuda.atomic.add(Arr, idx, t29)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[1]*d_i[2]*w_ik[i, k]*x_ik[i, k, 1]*x_ik[i, k, 2])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[1]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1])
                idx += 1
                cuda.atomic.add(Arr, idx, t20)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[2]*t16*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[1]*d_i[2]*t16*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, t21 + t22)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[2]*w_ik[i, k]*x_ik[i, k, 1]*x_ik[i, k, 2])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[1]*d_i[2]*w_ik[i, k]*x_ik[i, k, 1]*x_ik[i, k, 2])
                idx += 1
                cuda.atomic.add(Arr, idx, t32)
                idx += 1
                cuda.atomic.add(Arr, idx, t25)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[1]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 2])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 2])
                idx += 1
                cuda.atomic.add(Arr, idx, t26)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[1]*w_ik[i, k]*x_ik[i, k, 1]*x_ik[i, k, 2])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[2]*w_ik[i, k]*x_ik[i, k, 1]*x_ik[i, k, 2])
                idx += 1
                cuda.atomic.add(Arr, idx, t30 + t34)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[1]*t27*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[2]*t27*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[1]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 2])
                idx += 1
                cuda.atomic.add(Arr, idx, t28)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[1]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 2])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[1]*w_ik[i, k]*x_ik[i, k, 1]*x_ik[i, k, 2])
                idx += 1
                cuda.atomic.add(Arr, idx, t29)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[1]*d_i[2]*w_ik[i, k]*x_ik[i, k, 1]*x_ik[i, k, 2])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[1]*t27*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, t30 + t33)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[1]*d_i[2]*t27*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 2])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[1]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 2])
                idx += 1
                cuda.atomic.add(Arr, idx, t31)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[2]*w_ik[i, k]*x_ik[i, k, 1]*x_ik[i, k, 2])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[1]*d_i[2]*w_ik[i, k]*x_ik[i, k, 1]*x_ik[i, k, 2])
                idx += 1
                cuda.atomic.add(Arr, idx, t32)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[2]*t27*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[1]*d_i[2]*t27*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, t33 + t34)
                # %% summation over Arr. END

                t35 = t4*w_ik[i, k]*x_ik[i, k, 0]*2
                t36 = t4*w_ik[i, k]*x_ik[i, k, 1]*2
                t37 = t4*w_ik[i, k]*x_ik[i, k, 2]*2
                t38 = t6*w_ik[i, k]*x_ik[i, k, 0]*2
                t39 = t3*w_ik[i, k]*x_ik[i, k, 0]*2
                t40 = t6*w_ik[i, k]*x_ik[i, k, 1]*2
                t41 = t3*w_ik[i, k]*x_ik[i, k, 1]*2
                t42 = t6*w_ik[i, k]*x_ik[i, k, 2]*2
                t43 = t3*w_ik[i, k]*x_ik[i, k, 2]*2

                # %% summation over Atr
                idx = 0
                cuda.atomic.add(Atr, idx, t35 + t39)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[0]*d_i[1]*w_ik[i, k]*x_ik[i, k, 0]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[0]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[0]*d_i[1]*w_ik[i, k]*x_ik[i, k, 0]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, t35 + t38)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[1]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[0]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[1]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, t38 + t39)
                idx += 1
                cuda.atomic.add(Atr, idx, t36 + t41)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[0]*d_i[1]*w_ik[i, k]*x_ik[i, k, 1]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[0]*d_i[2]*w_ik[i, k]*x_ik[i, k, 1]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[0]*d_i[1]*w_ik[i, k]*x_ik[i, k, 1]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, t36 + t40)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[1]*d_i[2]*w_ik[i, k]*x_ik[i, k, 1]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[0]*d_i[2]*w_ik[i, k]*x_ik[i, k, 1]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[1]*d_i[2]*w_ik[i, k]*x_ik[i, k, 1]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, t40 + t41)
                idx += 1
                cuda.atomic.add(Atr, idx, t37 + t43)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[0]*d_i[1]*w_ik[i, k]*x_ik[i, k, 2]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[0]*d_i[2]*w_ik[i, k]*x_ik[i, k, 2]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[0]*d_i[1]*w_ik[i, k]*x_ik[i, k, 2]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, t37 + t42)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[1]*d_i[2]*w_ik[i, k]*x_ik[i, k, 2]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[0]*d_i[2]*w_ik[i, k]*x_ik[i, k, 2]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[1]*d_i[2]*w_ik[i, k]*x_ik[i, k, 2]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, t42 + t43)
                # %% summation over Atr. END

                t44 = t4*w_ik[i, k]
                t45 = t6*w_ik[i, k]
                t46 = t3*w_ik[i, k]

                # %% summation over Att
                idx = 0
                cuda.atomic.add(Att, idx, t44 + t46)
                idx += 1
                cuda.atomic.add(Att, idx, -d_i[0]*d_i[1]*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Att, idx, -d_i[0]*d_i[2]*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Att, idx, -d_i[0]*d_i[1]*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Att, idx, t44 + t45)
                idx += 1
                cuda.atomic.add(Att, idx, -d_i[1]*d_i[2]*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Att, idx, -d_i[0]*d_i[2]*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Att, idx, -d_i[1]*d_i[2]*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Att, idx, t45 + t46)
                # %% summation over Att. END

                # %% summation over br
                idx = 0
                cuda.atomic.add(br, idx,
                                d_i[1]*m_i[2]*w_ik[i, k]*x_ik[i, k, 0]*-2 + d_i[2]*m_i[1]*w_ik[i, k]*x_ik[i, k, 0]*2)
                idx += 1
                cuda.atomic.add(br, idx,
                                d_i[0]*m_i[2]*w_ik[i, k]*x_ik[i, k, 0]*2 - d_i[2]*m_i[0]*w_ik[i, k]*x_ik[i, k, 0]*2)
                idx += 1
                cuda.atomic.add(br, idx,
                                d_i[0]*m_i[1]*w_ik[i, k]*x_ik[i, k, 0]*-2 + d_i[1]*m_i[0]*w_ik[i, k]*x_ik[i, k, 0]*2)
                idx += 1
                cuda.atomic.add(br, idx,
                                d_i[1]*m_i[2]*w_ik[i, k]*x_ik[i, k, 1]*-2 + d_i[2]*m_i[1]*w_ik[i, k]*x_ik[i, k, 1]*2)
                idx += 1
                cuda.atomic.add(br, idx,
                                d_i[0]*m_i[2]*w_ik[i, k]*x_ik[i, k, 1]*2 - d_i[2]*m_i[0]*w_ik[i, k]*x_ik[i, k, 1]*2)
                idx += 1
                cuda.atomic.add(br, idx,
                                d_i[0]*m_i[1]*w_ik[i, k]*x_ik[i, k, 1]*-2 + d_i[1]*m_i[0]*w_ik[i, k]*x_ik[i, k, 1]*2)
                idx += 1
                cuda.atomic.add(br, idx,
                                d_i[1]*m_i[2]*w_ik[i, k]*x_ik[i, k, 2]*-2 + d_i[2]*m_i[1]*w_ik[i, k]*x_ik[i, k, 2]*2)
                idx += 1
                cuda.atomic.add(br, idx,
                                d_i[0]*m_i[2]*w_ik[i, k]*x_ik[i, k, 2]*2 - d_i[2]*m_i[0]*w_ik[i, k]*x_ik[i, k, 2]*2)
                idx += 1
                cuda.atomic.add(br, idx,
                                d_i[0]*m_i[1]*w_ik[i, k]*x_ik[i, k, 2]*-2 + d_i[1]*m_i[0]*w_ik[i, k]*x_ik[i, k, 2]*2)
                # %% summation over br END

                # %% summation over bt
                idx = 0
                cuda.atomic.add(bt, idx, d_i[1]*m_i[2]*w_ik[i, k]*-2 + d_i[2]*m_i[1]*w_ik[i, k]*2)
                idx += 1
                cuda.atomic.add(bt, idx, d_i[0]*m_i[2]*w_ik[i, k]*2 - d_i[2]*m_i[0]*w_ik[i, k]*2)
                idx += 1
                cuda.atomic.add(bt, idx, d_i[0]*m_i[1]*w_ik[i, k]*-2 + d_i[1]*m_i[0]*w_ik[i, k]*2)
                # %% summation over bt END

                cuda.atomic.add(c, 0, m_i[0]**2*w_ik[i, k] + m_i[1]**2*w_ik[i, k] + m_i[2]**2*w_ik[i, k])
            else:  # x_ik.shape[2] == 2

                t2 = x_ik[i, k, 0]**2
                t3 = d_i[1]**2
                t4 = d_i[2]**2
                t5 = t2*t4*w_ik[i, k]
                t6 = d_i[0]**2
                t7 = t4*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1]
                t8 = t2*t6*w_ik[i, k]
                t9 = t2*t3*w_ik[i, k]
                t10 = t6*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1]
                t11 = t3*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1]
                t12 = t7 + t11
                t13 = x_ik[i, k, 1]**2
                t14 = t7 + t10
                t15 = t4*t13*w_ik[i, k]
                t16 = t10 + t11
                t17 = t6*t13*w_ik[i, k]
                t18 = t3*t13*w_ik[i, k]

                # %% summation over Arr
                idx = 0
                cuda.atomic.add(Arr, idx, t5 + t9)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[1]*t2*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[2]*t2*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, t12)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[1]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1])
                idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[1]*t2*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, t5 + t8)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[1]*d_i[2]*t2*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[1]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1])
                idx += 1
                cuda.atomic.add(Arr, idx, t14)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[1]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1])
                idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[2]*t2*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[1]*d_i[2]*t2*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, t8 + t9)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[1]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1])
                idx += 1
                cuda.atomic.add(Arr, idx, t16)
                idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                idx += 1
                cuda.atomic.add(Arr, idx, t12)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[1]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1])
                idx += 1
                cuda.atomic.add(Arr, idx, t15 + t18)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[1]*t13*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[2]*t13*w_ik[i, k])
                idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[1]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1])
                idx += 1
                cuda.atomic.add(Arr, idx, t14)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[1]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[1]*t13*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, t15 + t17)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[1]*d_i[2]*t13*w_ik[i, k])
                idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[1]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*x_ik[i, k, 1])
                idx += 1
                cuda.atomic.add(Arr, idx, t16)
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[0]*d_i[2]*t13*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, -d_i[1]*d_i[2]*t13*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Arr, idx, t17 + t18)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Arr, idx, 0)
                # %% summation over Arr. END

                t19 = t4*w_ik[i, k]*x_ik[i, k, 0]*2
                t20 = t4*w_ik[i, k]*x_ik[i, k, 1]*2
                t21 = t6*w_ik[i, k]*x_ik[i, k, 0]*2
                t22 = t3*w_ik[i, k]*x_ik[i, k, 0]*2
                t23 = t6*w_ik[i, k]*x_ik[i, k, 1]*2
                t24 = t3*w_ik[i, k]*x_ik[i, k, 1]*2

                # %% summation over Atr
                idx = 0
                cuda.atomic.add(Atr, idx, t19 + t22)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[0]*d_i[1]*w_ik[i, k]*x_ik[i, k, 0]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[0]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[0]*d_i[1]*w_ik[i, k]*x_ik[i, k, 0]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, t19 + t21)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[1]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[0]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[1]*d_i[2]*w_ik[i, k]*x_ik[i, k, 0]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, t21 + t22)
                idx += 1
                cuda.atomic.add(Atr, idx, t20 + t24)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[0]*d_i[1]*w_ik[i, k]*x_ik[i, k, 1]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[0]*d_i[2]*w_ik[i, k]*x_ik[i, k, 1]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[0]*d_i[1]*w_ik[i, k]*x_ik[i, k, 1]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, t20 + t23)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[1]*d_i[2]*w_ik[i, k]*x_ik[i, k, 1]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[0]*d_i[2]*w_ik[i, k]*x_ik[i, k, 1]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, d_i[1]*d_i[2]*w_ik[i, k]*x_ik[i, k, 1]*-2)
                idx += 1
                cuda.atomic.add(Atr, idx, t23 + t24)
                # idx += 1
                # cuda.atomic.add(Atr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Atr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Atr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Atr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Atr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Atr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Atr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Atr, idx, 0)
                # idx += 1
                # cuda.atomic.add(Atr, idx, 0)
                # %% summation over Atr. END

                t25 = t4*w_ik[i, k]
                t26 = t6*w_ik[i, k]
                t27 = t3*w_ik[i, k]

                # %% summation over Att
                idx = 0
                cuda.atomic.add(Att, idx, t25 + t27)
                idx += 1
                cuda.atomic.add(Att, idx, -d_i[0]*d_i[1]*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Att, idx, -d_i[0]*d_i[2]*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Att, idx, -d_i[0]*d_i[1]*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Att, idx, t25 + t26)
                idx += 1
                cuda.atomic.add(Att, idx, -d_i[1]*d_i[2]*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Att, idx, -d_i[0]*d_i[2]*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Att, idx, -d_i[1]*d_i[2]*w_ik[i, k])
                idx += 1
                cuda.atomic.add(Att, idx, t26 + t27)
                # %% summation over Att. END

                # %% summation over br
                idx = 0
                cuda.atomic.add(br, idx, d_i[1]*m_i[2]*w_ik[i, k]*x_ik[i, k, 0]*-2 + d_i[2]*m_i[1]*w_ik[i, k]*x_ik[i, k, 0]*2)
                idx += 1
                cuda.atomic.add(br, idx, d_i[0]*m_i[2]*w_ik[i, k]*x_ik[i, k, 0]*2 - d_i[2]*m_i[0]*w_ik[i, k]*x_ik[i, k, 0]*2)
                idx += 1
                cuda.atomic.add(br, idx, d_i[0]*m_i[1]*w_ik[i, k]*x_ik[i, k, 0]*-2 + d_i[1]*m_i[0]*w_ik[i, k]*x_ik[i, k, 0]*2)
                idx += 1
                cuda.atomic.add(br, idx, d_i[1]*m_i[2]*w_ik[i, k]*x_ik[i, k, 1]*-2 + d_i[2]*m_i[1]*w_ik[i, k]*x_ik[i, k, 1]*2)
                idx += 1
                cuda.atomic.add(br, idx, d_i[0]*m_i[2]*w_ik[i, k]*x_ik[i, k, 1]*2 - d_i[2]*m_i[0]*w_ik[i, k]*x_ik[i, k, 1]*2)
                idx += 1
                cuda.atomic.add(br, idx, d_i[0]*m_i[1]*w_ik[i, k]*x_ik[i, k, 1]*-2 + d_i[1]*m_i[0]*w_ik[i, k]*x_ik[i, k, 1]*2)
                # idx += 1
                # cuda.atomic.add(br, idx, 0)
                # idx += 1
                # cuda.atomic.add(br, idx, 0)
                # idx += 1
                # cuda.atomic.add(br, idx, 0)
                # %% summation over br. END

                # %% summation over bt
                idx = 0
                cuda.atomic.add(bt, idx, d_i[1]*m_i[2]*w_ik[i, k]*-2 + d_i[2]*m_i[1]*w_ik[i, k]*2)
                idx += 1
                cuda.atomic.add(bt, idx, d_i[0]*m_i[2]*w_ik[i, k]*2 - d_i[2]*m_i[0]*w_ik[i, k]*2)
                idx += 1
                cuda.atomic.add(bt, idx, d_i[0]*m_i[1]*w_ik[i, k]*-2 + d_i[1]*m_i[0]*w_ik[i, k]*2)
                # %% summation over bt. END

                cuda.atomic.add(c, 0, m_i[0]**2*w_ik[i, k] + m_i[1]**2*w_ik[i, k] + m_i[2]**2*w_ik[i, k])


# %% Optimization on SO(3) manifold
def refine_pose_SO3(R: ndarray,
                    A: ndarray,
                    b: ndarray,
                    c: float,
                    max_iter: Optional[int] = 800,
                    verbose: Optional[int] = 0,
                    is_pose_reflected: Optional[bool] = False) -> ndarray:
    """Nonlinear search on SO(3) manifold to minimize objective function
    using gradient/newton descent and backtracking line search.

    Args:
        R: 3x3 rotation matrix.
        A: 9x9 matrix. part of objective function.
        b: 9x1 vector. part of objective function.
        c: scalar part of onjective function.
        max_iter: Maximum number of gradient calculations.
        verbose:
            0: Print nothing.
            1: Print final cost and norm of descent direction.
            2: Print for every step the cost, norm of descent direction
            and number of backtracking iterations.
        is_pose_reflected: Refine pose on O(3)\SO(3) manifold that contains
                           reflected rotations. This results in det(R) = -1.

    Returns:
        Rotation matrix.
    """

    # create some static matrices for faster and better understandable cost evaluation
    Q = np.zeros((9, 3))
    Q[5, 0] = 1
    Q[7, 0] = -1
    Q[2, 1] = -1
    Q[6, 1] = 1
    Q[1, 2] = 1
    Q[3, 2] = -1
    I = np.eye(3)

    # define objective function: r.T@A@r + 2*b.T@r + c
    def get_cost(R):
        return R.ravel(order='F').T@A@R.ravel(order='F') + 2*b.T@R.ravel(order='F') + c

    # define gradient and hessian
    def gradient_hessian(R):
        kron_R_I = np.kron(R, I)
        H1 = -np.kron(I, (A@R.ravel(order='F') + b.ravel()).reshape(3, 3, order='F')@R.T)
        H2 = kron_R_I@A@kron_R_I.T
        gradient = 2*Q.T@kron_R_I@(A@R.ravel(order='F') + b.ravel())
        hessian = 2*Q.T@(H1 + H2)@Q
        return gradient, hessian

    # initial cost
    cost_old = get_cost(R)

    if verbose == 2:
        print(f'´\nIteration  |      Cost     |   Norm of step    |    Line search iterations')

    # minimize objective function
    for j in range(max_iter):
        # get gradient and hessian on tangent space of manifold
        grad, hess = gradient_hessian(R)

        # calculate search direction
        tau = np.min(np.real(linalg.eigvals(hess)))
        tau = max(0, 1e-8 - tau)
        omega = -linalg.solve(hess + tau*np.eye(3), grad)  # newton direction
        # omega = - grad  # steepest descend

        # 1d backtracking linesearch in search direction
        c1 = 1e-4
        contraction = 0.7
        maxiter_line = 150
        alpha_init = 2.0*np.pi

        omega_norm = linalg.norm(omega)
        omega /= omega_norm

        # directional derivative at R along omega
        omegaf = omega.T@grad

        # initial stepsize
        alpha = alpha_init

        # retraction onto manifold using exponential map so(3) -> SO(3)
        R_new = exp_map_so3(alpha*omega)@R
        cost = get_cost(R_new)

        # backtracking until Armijo is satisfied
        for i in range(maxiter_line):
            if cost < cost_old + c1*omegaf*alpha:
                break
            # reduce stepsize
            alpha *= contraction
            # update R and cost
            R_new = exp_map_so3(alpha*omega)@R
            cost = get_cost(R_new)

        if cost < cost_old:
            # R = R_new
            # Force projection onto SO(3), to avoid numerical deviation from true rotation matrix
            U, _, Vh = linalg.svd(R_new)
            if is_pose_reflected:
                # don't enforce det(R) = 1
                R = U@Vh
            else:
                R = U@np.diag([1, 1, linalg.det(Vh@U)])@Vh
        # print(f'Cost: {cost}  |  Gradient norm: {omega_norm}')

        # Convergence?
        if (cost_old - cost < 1e-10) or (omega_norm < 1e-7):
            break
        cost_old = cost

        if verbose == 2:
            print(f'  {j:3.0f}      |   {cost:1.4e}  |   {omega_norm:1.4e}      |      {j}')

    if verbose > 0:
        print(f'Final cost: {cost:1.4e}')
        print(f'Norm of step: {omega_norm:1.4e}')

    return R


# %% Optimization on SE(3) manifold
def refine_pose_SE3(R: ndarray,
                    t: ndarray,
                    Arr: ndarray,
                    Att: ndarray,
                    Atr: ndarray,
                    br: ndarray,
                    bt: ndarray,
                    c: float,
                    max_iter: Optional[int] = 500,
                    verbose: Optional[int] = 0) -> Tuple[ndarray, ndarray]:
    """Nonlinear search on SO(3)xR3 manifold to minimize objective function
    using gradient/newton descent and backtracking line search.
    # TODO: check this... maybe not correct... convergence too slow. Need two step sizes for R,t?

    Args:
        R: 3x3 rotation matrix.
        Arr: 9x9 matrix part of objective function.
        Att: 3x3 matrix part of objective function.
        Atr: 3x9 matrix part of objective function.
        br: 9x1 matrix part of objective function.
        bt: 3x1 vector part of objective function.
        c: 1x1 scalar part of onjective function.
        max_iter: Maximum number of gradient calculations.
        verbose:
            0: Print nothing.
            1: Print final cost and norm of descent direction.
            2: Print for every step the cost, norm of descent direction
            and number of backtracking iterations.

    Returns:
        Rotation matrix and translation vector.
    """

    # create some static matrices for faster cost evaluation
    Q = np.zeros((9, 3))
    Q[5, 0] = 1
    Q[7, 0] = -1
    Q[2, 1] = -1
    Q[6, 1] = 1
    Q[1, 2] = 1
    Q[3, 2] = -1
    I = np.eye(3)

    # define objective function
    def get_cost(R, t):
        r = R.ravel(order='F')
        return r.T@Arr@r + t.T@Att@t + t.T@Atr@r + br.T@r + bt.T@t + c

    # define gradient and hessian
    def gradient_hessian(R, t):
        r = R.ravel(order='F')
        kron_R_I = np.kron(R, I)

        # partial derivatives of objective function
        fr = 2*Arr@r + Atr.T@t + br
        ft = 2*Att@t + Atr@r + bt
        frr = 2*Arr
        ftt = 2*Att
        ftr = Atr

        # gradient
        gradient = np.zeros(6)
        # rotation
        gradient[0:3] = Q.T@kron_R_I@fr
        # translation
        gradient[3:] = ft

        # hessian
        hessian = np.zeros((6, 6))

        mat = fr.reshape(3, 3, order='F')
        hessian[0:3, 0:3] = Q.T@(kron_R_I@frr@kron_R_I.T - np.kron(I, mat@R.T))@Q
        H = ftr@kron_R_I.T@Q
        hessian[3:6, 0:3] = H
        hessian[0:3, 3:6] = H.T
        hessian[3:6, 3:6] = ftt
        return gradient, hessian

    # initial cost
    cost_old = get_cost(R, t)

    if verbose == 2:
        print(f'´\nIteration  |      Cost     |   Norm of step    |    Line search iterations')

    # minimize objective function
    for j in range(max_iter):
        # get gradient and hessian on tangent space of manifold
        grad, hess = gradient_hessian(R, t)

        # calculate search direction
        tau = np.min(np.real(linalg.eigvals(hess)))
        tau = max(0, 1e-8 - tau)
        omega_v = -linalg.solve(hess + tau*np.eye(6), grad)  # newton direction
        # omega_v = - grad  # steepest descend

        # 1d backtracking linesearch in search direction
        c1 = 1e-4
        contraction = 0.7
        maxiter_line = 150
        alpha_init = 2.0*np.pi

        omega_v_norm = linalg.norm(omega_v)
        # omega_v /= omega_v_norm

        # directional derivative at R along omega_v
        omega_v_f = omega_v.T@grad

        # initial stepsize
        alpha = alpha_init

        # retraction onto manifold using exponential map so(3)xR3 ->
        R_new = exp_map_so3(alpha*omega_v[0:3])@R
        t_new = t + alpha*omega_v[3:]
        cost = get_cost(R_new, t_new)

        # backtracking until Armijo is satisfied
        for i in range(maxiter_line):
            if cost < cost_old + c1*omega_v_f*alpha:
                break
            # reduce stepsize
            alpha *= contraction
            # update R and cost
            R_new = exp_map_so3(alpha*omega_v[0:3])@R
            t_new = t + alpha*omega_v[3:6]
            cost = get_cost(R_new, t_new)

        if cost < cost_old:
            # R = R_new
            # Normalize, to avoid numerical deviation from true rotation matrix
            U, _, Vh = linalg.svd(R_new)
            R = U@np.diag([1, 1, linalg.det(Vh@U)])@Vh
            t = t_new
        # print(f'Cost: {cost}  |  Gradient norm: {omega_v_norm}')

        # Convergence?
        if (cost_old - cost < 1e-10) or (omega_v_norm < 1e-7):
            break
        cost_old = cost

        if verbose == 2:
            print(f'  {j:3.0f}      |   {cost:1.4e}  |   {omega_v_norm:1.4e}      |      {j}')

    if verbose > 0:
        print(f'Final cost: {cost:1.4e}')
        print(f'Norm of step: {omega_v_norm:1.4e}')

    return R, t


# %% Optimization on SO(3)xR3 manifold
def refine_pose_SO3xR3(R: ndarray,
                       t: ndarray,
                       Arr: ndarray,
                       Att: ndarray,
                       Atr: ndarray,
                       br: ndarray,
                       bt: ndarray,
                       c: float,
                       max_iter: Optional[int] = 500,
                       verbose: Optional[int] = 0) -> Tuple[ndarray, ndarray]:
    """Nonlinear search on SO(3)xR3 manifold to minimize objective function
    using gradient/newton descent and backtracking line search.
    # TODO: check this... maybe not correct... convergence too slow. Need two step sizes for R,t?

    Args:
        R: 3x3 rotation matrix
        Arr: 9x9 matrix part of objective function.
        Att: 3x3 matrix part of objective function.
        Atr: 3x9 matrix part of objective function.
        br: 9x1 matrix part of objective function.
        bt: 3x1 vector part of objective function.
        c: 1x1 scalar part of onjective function.
        max_iter: Maximum number of gradient calculations.
        verbose:
            0: Print nothing.
            1: Print final cost and norm of descent direction.
            2: Print for every step the cost, norm of descent direction
            and number of backtracking iterations.

    Returns:
        Rotation matrix and translation vector.
    """

    # create some static matrices for faster cost evaluation
    Q = np.zeros((9, 3))
    Q[5, 0] = 1
    Q[7, 0] = -1
    Q[2, 1] = -1
    Q[6, 1] = 1
    Q[1, 2] = 1
    Q[3, 2] = -1
    I = np.eye(3)

    # define objective function
    def get_cost(R, t):
        r = R.ravel(order='F')
        return r.T@Arr@r + t.T@Att@t + t.T@Atr@r + br.T@r + bt.T@t + c

    # define gradient and hessian of rotation part
    def gradient_hessian_r(R, t):
        r = R.ravel(order='F')
        kron_R_I = np.kron(R, I)

        # partial derivatives of objective function
        fr = 2*Arr@r + Atr.T@t + br
        frr = 2*Arr
        mat = fr.reshape(3, 3, order='F')

        gradient = Q.T@kron_R_I@fr
        hessian = Q.T@(kron_R_I@frr@kron_R_I.T - np.kron(I, mat@R.T))@Q

        return gradient, hessian

    # define gradient and hessian of translation part
    def gradient_hessian_t(R, t):
        r = R.ravel(order='F')

        # partial derivatives of objective function
        ft = 2*Att@t + Atr@r + bt
        ftt = 2*Att

        gradient = ft
        hessian = ftt

        return gradient, hessian

    # initial cost
    cost_old1 = get_cost(R, t)

    if verbose == 2:
        print(f'´\nIteration  |      Cost     |   Norm of step    |    Line search iterations')

    # minimize objective function
    for j in range(max_iter):
        # Minimize rotation part
        # get gradient and hessian on tangent space of SO(3) manifold
        grad, hess = gradient_hessian_r(R, t)

        # calculate search direction 'omega'
        tau = np.min(np.real(linalg.eigvals(hess)))
        tau = max(0, 1e-8 - tau)
        omega = -linalg.solve(hess + tau*np.eye(3), grad)  # newton direction
        # omega = -grad  # steepest descend

        # 1d backtracking linesearch in search direction
        c1 = 1e-4
        contraction = 0.7
        maxiter_line = 150
        alpha_init = 2.0*np.pi

        omega_norm = linalg.norm(omega)
        omega /= omega_norm

        # directional derivative at R along omega
        omegaf = omega.T@grad

        # initial stepsize
        alpha = alpha_init

        # retraction onto manifold using exponential map so(3) -> SO(3)
        R_new = exp_map_so3(alpha*omega)@R
        cost = get_cost(R_new, t)

        # backtracking until Armijo is satisfied
        for i in range(maxiter_line):
            if cost < cost_old1 + c1*omegaf*alpha:
                break
            # reduce stepsize
            alpha *= contraction
            # update R and cost
            R_new = exp_map_so3(alpha*omega)@R
            cost = get_cost(R_new, t)

        if cost < cost_old1:
            # Normalize, to avoid numerical deviation from true rotation matrix
            U, _, Vh = linalg.svd(R_new)
            R = U@np.diag([1, 1, linalg.det(Vh@U)])@Vh

        cost_old2 = cost

        # Minimize translation part
        # get gradient and hessian
        grad, hess = gradient_hessian_t(R, t)

        # calculate search direction 'v'
        tau = np.min(np.real(linalg.eigvals(hess)))
        tau = max(0, 1e-8 - tau)
        v = -linalg.solve(hess + tau*np.eye(3), grad)  # newton direction
        # v = -grad  # steepest descend

        # 1d backtracking linesearch in search direction
        c1 = 1e-4
        contraction = 0.7
        maxiter_line = 150
        alpha_init = 5

        v_norm = linalg.norm(v)

        # directional derivative at t along v
        vf = v.T@grad

        # initial stepsize
        alpha = alpha_init

        # calculate initial cost
        t_new = t + alpha*v
        cost = get_cost(R, t_new)

        # backtracking until armijo is satisfied
        for i in range(maxiter_line):
            if cost < cost_old2 + c1*vf*alpha:
                break
            # reduce stepsize
            alpha *= contraction
            # update t and cost
            t_new = t + alpha*v
            cost = get_cost(R, t_new)

        if cost < cost_old2:
            t = t_new

        # Convergence?
        if (cost_old1 - cost < 1e-10) or (omega_norm + v_norm < 1e-7):
            break
        cost_old1 = cost

        if verbose == 2:
            print(f'  {j:3.0f}      |   {cost:1.4e}  |   {omega_norm + v_norm:1.4e}      |      {j}')

    if verbose > 0:
        print(f'Final cost: {cost:1.4e}')
        print(f'Norm of step: {omega_norm + v_norm:1.4e}')

    return R, t


# %% Exponentiel maps and retraction onto manifolds
def exp_map_so3(omega: ndarray) -> ndarray:
    """calculates exponential map R3->so(3)->SO(3)

    Args:
        omega: 3x1 vector representing local tangent space so(3) of SO(3).

    Returns:
        3x3 matrix as retraction onto SO(3).
    """

    # if omega.ndim == 2:  # multiple omega vectors

    # get rotation angle
    theta = np.linalg.norm(omega)
    # Calc screw matrix [omega]x = Omega_x
    Omega_x = np.zeros((3, 3))
    Omega_x[1, 0] = omega[2]
    Omega_x[2, 0] = -omega[1]
    Omega_x[0, 1] = -omega[2]
    Omega_x[2, 1] = omega[0]
    Omega_x[0, 2] = omega[1]
    Omega_x[1, 2] = -omega[0]

    # return np.eye(3) + Omega_x*np.sin(theta)/theta + Omega_x@Omega_x*(1 - np.cos(theta))/theta**2
    return np.eye(3) + Omega_x*np.sinc(theta/np.pi) + Omega_x@Omega_x*0.5*np.sinc(theta/2/np.pi)**2


def log_map_SO3(R):
    """Calculates logarithmic map SO(3)->so(3)->R3

    Args:
        R: 3x3 matrix representing SO(3).

    Returns:
        3x1 vector representing local tangent space in so(3).
    """

    if R.ndim == 3:  # multiple matrices
        omega = np.zeros((R.shape[0], 3))
        for k in range(R.shape[0]):
            theta = np.arccos(np.clip((np.trace(R[k, :, :]) - 1.0)/2.0, -1, 1))
            log_R = 0.5/np.sinc(theta/np.pi)*(R[k, :, :].T - R[k, :, :])
            omega[k, :] = 0.5*np.asarray([log_R[1, 2] - log_R[2, 1],
                                          log_R[2, 0] - log_R[0, 2],
                                          log_R[0, 1] - log_R[1, 0]])
    else:
        theta = np.arccos(np.clip((np.trace(R) - 1.0)/2.0, -1, 1))
        log_R = 0.5/np.sinc(theta/np.pi)*(R.T - R)
        omega = 0.5*np.asarray([log_R[1, 2] - log_R[2, 1],
                                log_R[2, 0] - log_R[0, 2],
                                log_R[0, 1] - log_R[1, 0]])
    return omega


def exp_map_se3(omega, v):
    # TODO: Check all of this! is this correct??

    """Calculates exponential map from R6->se(3)->SE(3)

    Args:
        omega: 3x1 vector representing local tangent space so(3) of SO(3).
        v:

    Returns:
        4x4 matrix as retraction onto SE(3).
    """
    # TODO: check normalization! is not necessary?? singularities...
    # normalize omega
    ww = np.linalg.norm(omega)
    omega = omega/ww
    # Calc screw matrix [omega]x = Omega
    Omega = np.zeros((3, 3))
    Omega[1, 0] = omega[2]
    Omega[2, 0] = -omega[1]
    Omega[0, 1] = -omega[2]
    Omega[2, 1] = omega[0]
    Omega[0, 2] = omega[1]
    Omega[1, 2] = -omega[0]
    return (np.eye(3) + (1 - np.cos(ww))/ww**2*Omega + (ww - np.sin(ww))/ww**3*Omega@Omega)@v
