# -*- coding: utf-8 -*-1.3
# Copyright (C) 2021  David Uhlig
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import setuptools
from setuptools import setup

# Load README as full description
short_description = "Code for Generic Camera Calibration."
try:
    with open("README.md", "r") as f:
        long_description = f.read()
except:
    long_description = short_description



setup(
    python_requires='>=3.6',
    name='generic_camera_calibration',
    description=short_description,
    long_description=long_description,
    project_urls={
        'Source': 'https://gitlab.com/iiit-public/generic-camera-calibration',
    },
    author='David Uhlig',
    author_email='uhlig@kit.edu',
    install_requires=[
        'numPy',
        'scipy',
        'matplotlib',
        'numba',
        'tqdm',
    ],
    license='GNU General Public License v3 (GPLv3)',
    packages=setuptools.find_packages(),
    zip_safe=True)
