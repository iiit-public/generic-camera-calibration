# Copyright (C) 2021  David Uhlig
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import matplotlib.pyplot as plt
import numpy as np

from generic_camera_calibration import calibration_utilities as utilities
from generic_camera_calibration import camera_calibration as calibration
from generic_camera_calibration.calibration_utilities import eul2rot

#%% Setup parameters
# load example data: ideal light field camera: interpretable as 5x5 multi-camera
# array where each camera has a resolution of 128x128
path = 'examples/data/Lightfield_5x5x128x128/'

num_iterations = 100
add_noise = True
use_cuda = True
use_acceleration = True


#%% Read calibration data
x_ik, w_ik, C_ik, R_k, t_k, mask, sensor_shape = \
    utilities.read_calibration_data(path=path, file_name='registration_data')


#%%
# Add noise to initial pose (for testing...)
if add_noise:
    # np.random.seed(3)
    for k in range(R_k.shape[0]):
        eul = 10*np.random.randn(3, 1)
        # eul = np.asarray([180,0,0])
        R_error = eul2rot(eul*np.pi/180)
        R_k[k, :, :] = R_error@R_k[k, :, :]
        t_error = 0.1*np.random.randn(3, 1)
        t_k[k, :, :] += t_error

# Keep start as reference
R_k_start = R_k.copy()
t_k_start = t_k.copy()

#%% Start calibration
result = calibration.calibrate(x_ik, w_ik, R_k=R_k, t_k=t_k,
                               max_num_iterations=num_iterations, use_cuda=use_cuda,
                               use_acceleration=use_acceleration, calc_errors=True,
                               save_errors=True, show_progress=False)
R_k = result["R_k"]
t_k = result["t_k"]
r_i = result["r_i"]
mean_euclidean_distance = result["mean_euclidean_error"]
mean_weighted_distance = result["mean_weighted_error"]
error = result["error"]

print(f'Calibration result:')
print(f'Euclidean distance:')
print(f'Mean: {np.mean(error)*1e6:2.4f}µm, '
      f'RMSE: {np.sqrt(np.mean(error**2))*1e6:2.4f}µm')
print(f'Weighted euclidean distance:')
print(f'Mean: {np.mean(error*w_ik)/np.mean(w_ik)*1e6:2.4f}µm, '
      f'RMSE: {np.sqrt(np.mean((w_ik*error)**2)/np.mean(w_ik**2))*1e6:2.4f}µm')


#%% Plot results
utilities.show_result(r_i, x_ik, R_k, t_k, w_i=w_ik.mean(axis=1), length_ray=50, show_pose=False,
                      show_rays=False, show_reference=True, num_rays=1000, figure_name='Estimated pose')
utilities.show_result(r_i, x_ik, R_k, t_k, w_i=w_ik.mean(axis=1), length_ray=500,
                      show_rays=True, show_reference=False, num_rays=500, figure_name='Estimated rays')
plt.show()

# normalize for better visualization
r_norm = utilities.normalize_coordinate_system(r_i, R_k, t_k, w_i=w_ik.mean(axis=1), sensor_shape=sensor_shape)


utilities.show_result(r_norm, x_ik, R_k, t_k, num_rays=1000, length_ray=0.1,
                      show_rays=True, show_reference=False, figure_name='Estimated rays')
utilities.show_result(r_norm, x_ik, R_k, t_k, num_rays=1000,
                      show_rays=False, show_reference=True, figure_name='Estimated pose')

# show starting configuration
utilities.show_result(r_i, x_ik, R_k_start, t_k_start, num_rays=1000,
                      show_rays=False, show_reference=True, figure_name='Start pose')
plt.show()


#%% Error analysis
plt.figure('Error Analysis')
plt.subplot(2, 1, 1)
plt.title('Mean euclidean distance')
plt.plot(mean_euclidean_distance, 'b')
plt.yscale('log')
plt.subplot(2, 1, 2)
plt.title('Weighted mean euclidean distance')
plt.plot(mean_weighted_distance, 'b')
plt.yscale('log')
plt.show()
