# A Calibration Method for the Generalized Imaging Model with Uncertain Calibration Target Coordinates

This is Python code to calibrate a camera using the generic imaging model. 


## Description
In a generic camera calibration, each pixel is assigned a sight ray and the geometric parameters of each ray are 
calibrated individually.

The presented method calibrates the pixels of any camera by minimizing the point-ray distance between the 
respective rays and observed reference points. For this purpose, reference points should be generated, e.g. 
by means of active pattern sequences displayed on a monitor.
For example, phase shift coding not only provides the local reference coordinate, but also a measure of the 
uncertainty of the estimation.

The absolute pose of the reference target is generally unknown. As usual for camera calibration, the reference 
target is therefore observed from different poses and reference points (+ the uncertainty) are measured each time. 
The presented method now alternately optimizes the camera rays and the poses of the reference targets.

As input data for the method the reference points (+uncertainty) are needed and also an initialization of the 
reference target poses.

The output of the method are the optimized camera rays and the optimized poses of the reference targets.


## License and Usage

This software is licensed under the GNU GPLv3 license (see below).

If you use this software in your scientific research, please cite [our paper](https://doi.org/10.1007/978-3-030-69535-4_33):

    @InProceedings{Uhlig_2020_ACCV,
        author    = {Uhlig, David and Heizmann, Michael},
        title     = {A Calibration Method for the Generalized Imaging Model with Uncertain Calibration Target Coordinates},
        booktitle = {Proceedings of the Asian Conference on Computer Vision (ACCV)},
        month     = {November},
        year      = {2020}
    }


## Quick Start
Have a look at the examples folder.


## How to use

### Data format
In order to use the calibration, the input data must be converted into a suitable format.

In the following, ``I`` is the number of pixels to be calibrated and ``K`` is the number of poses.

The coordinates of the reference target are stored in the array ``x_ik`` and must have the dimension ``[I, K, 3]`` 
or ``[I, K, 2]``, depending on whether the reference target is planar and provides only local 2D coordinates or 
whether it provides 3D coordinates.

If weighting factors ``w_ik`` are used, they must have the dimension ``[I, K]``.

The method can be initialized either with an estimate of the rays or with an estimate of the poses.

The rays ``r_i`` have the dimension ``[I, 6]``. To each pixel 6 ray parameters are assigned in Plücker 
coordinates. The first 3 values are the vector of the ray direction, the other 3 are the ray moment vector.

The reference pose is represented by a rotation matrix and a translation vector per pose. The array of 
rotation matrices ``R_k`` must have the dimension ``[K, 3, 3]``. The array of translation vectors ``t_k`` must 
have the dimension ``[K, 3, 1]``.

### Code
The framework provides mainly three functions to be used:

#### Camera Calibration:
* ``camera_calibration.calibrate(...)``: 
    Implementation of the alternating minimization. Iteratively calls the ray estimation and pose estimation.
    Input are the reference target coordinates ``x_ik``. Additionally, an array of weight factors ``w_ik`` can be 
    provided. If ``r_i`` is ``None``, the alternating minimization will be initialized with the given pose estimate
    ``R_k`` and ``t_k``. If no pose is provided the pose is randomly selected. A ray input is prioritized 
    over a pose input. \
    The function returns a dictionary containing the optimal ray parameters ``dict["r_i"]``, the optimal pose ``dict["R_k"]``
    and ``dict["t_k"]``, and if needed the calibration error ``dict["error"]``.
```
result = calibration.calibrate(x_ik, w_ik=w_ik, R_k=R_k, t_k=t_k, r_i=r_i)
```

#### Pose estimation:
``pose_estimation.estimate(...)`` estimates the pose of the reference target. The input pose ``R_k`` and 
``t_k`` are modified and updated by the function.
  
```
pose_estimation.estimate(r_i, x_ik, w_ik, R_k, t_k)
```

#### Ray estimation:
* ``camera_ray_estimation.estimate(...)`` estimates the camera rays. The input rays ``r_i`` 
  are modified and updated by the function.
  
```
camera_ray_estimation.estimate(r_i, x_ik, w_ik, R_k, t_k)
```

#### Visualize the results:
````
calibration_utilities.show_result(r_i, x_ik, R_k, t_k, num_rays=1000, show_rays=False, show_reference=True,  figure_name='Estimated pose')
calibration_utilities.show_result(r_i, x_ik, R_k, t_k, length_ray=500, num_rays=500, show_rays=True, show_reference=False,  figure_name='Estimated rays')
````  


#### Note
When using the GPU implementation it is advisable to use a 64bit float representation for the ray 
parameter array ``r_i`` to avoid numerical issues.

When there isn't enough VRAM on the GPU, one can use only a part of the pixels (e.g. the ones with the 
lowest uncertainty) to estimate the reference poses. When the alternating minimization converges, one
can use the obtained poses to estimate the ray parameters of the remaining pixels.


## License

Copyright (C) 2018-2021  David Uhlig

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
